include(subpackage)

# define the cheetah libraries
set(CHEETAH_LIBRARIES ${PROJECT_NAME} ${DEPENDENCY_LIBRARIES})

# define the cheetah test_utils libraries
set(CHEETAH_TEST_UTILS ${PROJECT_NAME}_test_utils)
list(APPEND CHEETAH_TEST_UTILS ${PANDA_TEST_LIBRARIES})

# ensure we can access our headers with #include "cheetah/...."
include_directories(..)
include_directories(${PROJECT_BINARY_DIR})

# subpackages to define ${lib_src} & ${test_utils_src} lists
# in dependency order
SUBPACKAGE(utils) # not dependent on any other subpackage
SUBPACKAGE(channel_mask)
SUBPACKAGE(cuda_utils)
SUBPACKAGE(data)
SUBPACKAGE(sigproc)
SUBPACKAGE(psrdada)
SUBPACKAGE(emulator)
SUBPACKAGE(generators)
SUBPACKAGE(rcpt)
SUBPACKAGE(rcpt_low)
SUBPACKAGE(brdz)
SUBPACKAGE(cxft)
SUBPACKAGE(ddtr)
SUBPACKAGE(fdao)
SUBPACKAGE(fdas)
SUBPACKAGE(fft)
SUBPACKAGE(fldo)
SUBPACKAGE(hrms)
SUBPACKAGE(producers)
SUBPACKAGE(psbc)
SUBPACKAGE(pwft)
SUBPACKAGE(rfim)
SUBPACKAGE(sift)
SUBPACKAGE(sps)
SUBPACKAGE(sps_clustering)
SUBPACKAGE(spsift)
SUBPACKAGE(tdao)
SUBPACKAGE(tdrt)
SUBPACKAGE(dred)
SUBPACKAGE(tdas)
SUBPACKAGE(exporters)
SUBPACKAGE(pipeline)
SUBPACKAGE(candidate_pipeline)

# should come after all SUBPACKGE directives
include_subpackage_files()

# -- the main cuda library
if(ENABLE_CUDA)
  cuda_compile(cuda_objects ${lib_src_cuda})
  list(APPEND cuda_objects ${lib_obj_cuda})
  cuda_generate_link_file(cuda_link_file ${PROJECT_NAME} ${cuda_objects})
  list(APPEND cuda_objects ${cuda_link_file})
endif(ENABLE_CUDA)

# -- the main library target
add_library(${PROJECT_NAME} ${lib_src_cpu} ${cuda_objects})
if(ENABLE_CUDA)
  cuda_add_cufft_to_target(${PROJECT_NAME})
  target_link_libraries(${PROJECT_NAME}
        ${CUDA_LIBRARIES}
  )
  # We need to set the linker language based on what the expected generated file
  # would be. CUDA_C_OR_CXX is computed based on CUDA_HOST_COMPILATION_CPP.
  set_target_properties(${cuda_target}
          PROPERTIES
          LINKER_LANGUAGE ${CUDA_C_OR_CXX}
          )
endif(ENABLE_CUDA)

# test_utils library target
add_library(${PROJECT_NAME}_test_utils ${test_utils_src_cpu})
target_include_directories(${PROJECT_NAME}_test_utils PUBLIC ${GTEST_INCLUDE_DIR})

install(TARGETS ${PROJECT_NAME} ${PROJECT_NAME}_test_utils DESTINATION ${LIBRARY_INSTALL_DIR})

