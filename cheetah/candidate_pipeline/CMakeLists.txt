set(module_candidate_pipeline_lib_src_cpu
    src/Config.cpp
    PARENT_SCOPE
)

TEST_UTILS()

add_subdirectory(test)

add_executable(cheetah_candidate_pipeline src/main.cpp)
target_link_libraries(cheetah_candidate_pipeline ${CHEETAH_LIBRARIES})
