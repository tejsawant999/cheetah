set(module_channel_mask_lib_src_cpu
    src/Config.cpp
    src/ChannelRangeConfig.cpp
    src/FlaggedChannels.cpp
    PARENT_SCOPE
)

TEST_UTILS()

add_subdirectory(test)
