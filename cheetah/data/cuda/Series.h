#ifndef SKA_CHEETAH_DATA_CUDA_SERIES_H
#define SKA_CHEETAH_DATA_CUDA_SERIES_H
#ifdef ENABLE_CUDA
#include "cheetah/data/Series.h"
#include "cheetah/cuda_utils/cuda_thrust.h"
#include <panda/arch/nvidia/DeviceCopy.h>

namespace ska {
namespace cheetah {
namespace data {


template <typename ValueType, typename Alloc>
class Series<cheetah::Cuda, ValueType, Alloc>: public VectorLike<thrust::device_vector<ValueType,Alloc>>
{
#ifdef __CUDACC__
#if __CUDACC_VER_MAJOR__ > 10 || (__CUDACC_VER_MAJOR__ == 10 && __CUDACC_VER_MINOR__ >= 2)
#define __NVCC_THRUST_SUPPORTS_ALLOCATOR_CONSTRUCTORS__
#endif
        typedef VectorLike<thrust::device_vector<ValueType,Alloc>> BaseT;

    public:
        typedef cheetah::Cuda ArchitectureType;
        typedef Alloc Allocator;

    public:
        Series(std::size_t size)
            : BaseT(size)
        {
        }

        template<typename OtherArch, typename OtherAlloc>
        Series(Series<OtherArch, ValueType, OtherAlloc> const& copy)
            : BaseT(copy.size())
        {
            panda::copy(copy.begin(), copy.end(), this->begin());
        }

        Series(std::size_t size, Allocator const& allocator)
#ifdef __NVCC_THRUST_SUPPORTS_ALLOCATOR_CONSTRUCTORS__
            : BaseT(size, allocator)
        {
#else // __NVCC_THRUST_SUPPORTS_ALLOCATOR_CONSTRUCTORS__
            : BaseT(size)
        {
		(void) allocator;
#endif // __NVCC_THRUST_SUPPORTS_ALLOCATOR_CONSTRUCTORS__
        }


        /**
         * @brief copy constructor. Series data transfered from Series data on a device
         */
        template<typename OtherArch, typename OtherAlloc>
        Series(Series<OtherArch, ValueType, OtherAlloc> const& copy, Alloc const& allocator=Alloc())
#ifdef __NVCC_THRUST_SUPPORTS_ALLOCATOR_CONSTRUCTORS__
            : BaseT(copy.size(), allocator)
        {
#else // __NVCC_THRUST_SUPPORTS_ALLOCATOR_CONSTRUCTORS__
            : BaseT(copy.size())
        {
	    (void) allocator;
#endif // __NVCC_THRUST_SUPPORTS_ALLOCATOR_CONSTRUCTORS__
            thrust::copy(copy.begin(), copy.end(), this->begin());
        }
#endif // __CUDACC__
};

} // namespace data
} // namespace cheetah
} // namespace ska

#endif //ENABLE_CUDA
#endif // SKA_CHEETAH_DATA_CUDA_SERIES_H
