/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/TimeFrequencyStats.h"
#include <algorithm>


namespace ska {
namespace cheetah {
namespace data {

template<typename TimeFrequencyType>
TimeFrequencyStats<TimeFrequencyType>::Statistics::Statistics()
    : mean(0)
    , variance(0)
{
}

template<typename TimeFrequencyType>
TimeFrequencyStats<TimeFrequencyType>::TimeFrequencyStats(std::shared_ptr<TimeFrequencyType> const& ptr)
    : BaseT(std::move(ptr))
{
}

template<typename TimeFrequencyType>
TimeFrequencyStats<TimeFrequencyType>::~TimeFrequencyStats()
{
}

template<typename TimeFrequencyType>
void TimeFrequencyStats<TimeFrequencyType>::calculate_stats() const
{
    const std::size_t nspectra=this->get().number_of_spectra();
    const std::size_t nchans=this->get().number_of_channels();
    _channel_stats.assign(nchans, Statistics());
    _spectrum_stats.assign(nspectra, Statistics());

    auto it=this->get().begin();
    auto end=this->get().end();
    typename TimeFrequencyType::ConstIterator chan_end;
    auto spectrum_stats_it = _spectrum_stats.begin();
    do {
        chan_end=it+nchans;
        auto& spectrum_stat = *spectrum_stats_it;
        auto channel_stat_it = _channel_stats.begin();
        //boost::accumulators::accumulator_set<NumericalRep, boost::accumulators::stats<boost::accumulators::tag::median>> spectrum_acc;
        while(it!=chan_end) {
            double val = static_cast<double>(*it);

            // mean
            spectrum_stat.mean += val/(double)nchans;

            auto& channel_stat = *channel_stat_it;
            channel_stat.mean += val/(double)nspectra;

            // variance
            const double v_squared = (double)(val * val);
            spectrum_stat.variance += v_squared/(double)nchans;
            channel_stat.variance += v_squared/(double)nspectra;

            // median
            //spectrum_acc(*it);

            ++it;
            ++channel_stat_it;
        }
        //spectrum_stat.median = boost::accumulators::median(spectrum_acc);
        spectrum_stat.variance -= (spectrum_stat.mean * spectrum_stat.mean);
        ++spectrum_stats_it;
    } while(it != end);

    // store results
    auto channel_stats_it = _channel_stats.begin();
    while(channel_stats_it != _channel_stats.end())
    {
        (*channel_stats_it).variance -= ((*channel_stats_it).mean * (*channel_stats_it).mean);
        ++channel_stats_it;
    }

}

template<typename TimeFrequencyType>
std::vector<typename TimeFrequencyStats<TimeFrequencyType>::Statistics> const& TimeFrequencyStats<TimeFrequencyType>::channel_stats() const
{
    if(_channel_stats.size() != this->template dimension<data::Frequency>()) {
        calculate_stats();
    }
    return _channel_stats;
}

template<typename TimeFrequencyType>
std::vector<typename TimeFrequencyStats<TimeFrequencyType>::Statistics> const& TimeFrequencyStats<TimeFrequencyType>::spectrum_stats() const
{
    if(_spectrum_stats.size() != this->template dimension<data::Time>()) {
        calculate_stats();
    }
    return _spectrum_stats;
}

} // namespace data
} // namespace cheetah
} // namespace ska
