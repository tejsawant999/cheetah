/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/test/FrequencyTimeTest.h"
#include "cheetah/data/FrequencyTime.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/test_utils/FrequencyTimeTester.h"
#include "cheetah/utils/ModifiedJulianClock.h"
#include <boost/units/systems/si/prefixes.hpp>
#include <chrono>

namespace ska {
namespace cheetah {
namespace data {
namespace test {


FrequencyTimeTest::FrequencyTimeTest()
    : ::testing::Test()
{
}

FrequencyTimeTest::~FrequencyTimeTest()
{
}

void FrequencyTimeTest::SetUp()
{
}

void FrequencyTimeTest::TearDown()
{
}

TEST_F(FrequencyTimeTest, test_copy)
{

    data::DimensionSize<data::Time> number_of_spectra(24U);
    data::DimensionSize<data::Frequency> number_of_channels(5U);

    data::FrequencyTime<Cpu, uint8_t> ft(number_of_channels, number_of_spectra);
    auto f1 =  data::FrequencyTime<Cpu, uint8_t>::FrequencyType(5.0 * boost::units::si::hertz);
    auto f2 =  data::FrequencyTime<Cpu, uint8_t>::FrequencyType(12.0 * boost::units::si::hertz);
    auto delta = (f2 - f1)/ (double)number_of_channels;
    ft.set_channel_frequencies_const_width( f1, delta );
    data::FrequencyTime<Cpu, uint16_t> ft2(ft); // the copy
    ASSERT_EQ(ft2.number_of_channels(), ft.number_of_channels());
    ASSERT_EQ(ft2.number_of_spectra(), ft.number_of_spectra());
    ASSERT_EQ(ft2.channel_frequencies().size(), ft.channel_frequencies().size());
    for(std::size_t i = 0U; i < number_of_channels; ++i) {
        ASSERT_EQ(ft2.channel_frequencies()[i], ft.channel_frequencies()[i]);
    }
}

// use the FrequencyTimeTester to test most interfaces
typedef ::testing::Types<FrequencyTimeTesterTraits<data::FrequencyTime<Cpu, uint8_t>>
                        ,FrequencyTimeTesterTraits<data::FrequencyTime<Cpu, float>>> FrequencyTimeDataTypes;
INSTANTIATE_TYPED_TEST_CASE_P(TimeFrequenceyTests, FrequencyTimeTester, FrequencyTimeDataTypes);

// check dimension query structs work
static_assert(pss::astrotypes::has_dimensions<FrequencyTime<Cpu, uint8_t>, data::Frequency, data::Time>::value, "expecting true");
static_assert(pss::astrotypes::has_exact_dimensions<FrequencyTime<Cpu, float>, data::Frequency, data::Time>::value, "expecting true");
static_assert(!pss::astrotypes::has_exact_dimensions<FrequencyTime<Cpu, uint8_t>, data::Time, data::Frequency>::value, "expecting false");

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
