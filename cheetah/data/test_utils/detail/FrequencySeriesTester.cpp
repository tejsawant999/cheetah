/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/test_utils/FrequencySeriesTester.h"
#include "cheetah/data/FrequencySeries.h"
#include <panda/TypeTraits.h>


namespace ska {
namespace cheetah {
namespace data {
namespace test {

namespace {
    template<class Alloc, typename DeviceType, typename Enable>
    struct ConstructAllocatorHelper;

    template<class Alloc, typename DeviceType>
    struct ConstructAllocatorHelper<Alloc, DeviceType, std::true_type>
    {
          static inline
          Alloc construct(DeviceType& device)
          {
              return Alloc(device);
          }
    };

    template<class Alloc, typename DeviceType>
    struct ConstructAllocatorHelper<Alloc, DeviceType, std::false_type> {
          static inline
          Alloc construct(DeviceType&)
          {
              return Alloc();
          }
    };
    template<class Alloc, typename DeviceType>
    struct ConstructAllocator
    {
      private:
          template<typename T>
          using HasDeviceConstructorT = decltype(T(std::declval<DeviceType&>()));

      public:
          static inline
          Alloc construct(DeviceType& device)
          {
              return ConstructAllocatorHelper<Alloc, DeviceType, typename std::conditional<panda::HasMethod<Alloc,HasDeviceConstructorT>::value, std::true_type, std::false_type>::type>::construct(device);
          }
   };
}// namespace

template<typename FrequencySeriesT>
typename FrequencySeriesTesterTraits<FrequencySeriesT>::Allocator FrequencySeriesTesterTraits<FrequencySeriesT>::allocator(panda::PoolResource<Arch>& device)
{
    return ConstructAllocator<Allocator, panda::PoolResource<Arch>>::construct(device);
}

template<typename FrequencySeriesTesterTraitsT>
FrequencySeriesTester<FrequencySeriesTesterTraitsT>::FrequencySeriesTester()
{
}

TYPED_TEST_P(FrequencySeriesTester, construct_resize)
{
    typedef TypeParam Traits;
    typedef typename Traits::FrequencySeriesType FrequencySeriesType;

    Traits traits;
    for(auto& device : this->_system.devices()) {
        FrequencySeriesType series(traits.allocator(*device));
        ASSERT_EQ(series.size(), std::size_t(0));
        // increase size
        series.resize(2);
        ASSERT_EQ(series.size(), std::size_t(2));
        // reduce ize
        series.resize(1);
        ASSERT_EQ(series.size(), std::size_t(1));
        // resize to exisiting size should do nothing
        auto it = series.begin();
        series.resize(1);
        ASSERT_EQ(series.size(), std::size_t(1));
        ASSERT_EQ(it, series.begin());
    }
}

TYPED_TEST_P(FrequencySeriesTester, begin_end)
{
    typedef TypeParam Traits;
    typedef typename Traits::FrequencySeriesType FrequencySeriesType;

    Traits traits;
    std::size_t n=2;
    for(auto& device : this->_system.devices()) {
        FrequencySeriesType series(n, traits.allocator(*device));

        // begin end on non-const object
        std::size_t count=0;
        {
            auto it = series.begin();
            ASSERT_EQ(n, std::size_t(std::distance(it, series.end())));
            while(it != series.end()) {
                ++count;
                ++it;
            }
            ASSERT_EQ(count, n);
        }

        // begin end on const object
        const FrequencySeriesType& const_series = series;
        {
            count=0;
            auto it = const_series.begin();
            ASSERT_EQ(n, std::size_t(std::distance(it, const_series.end())));
            while(it != const_series.end()) {
                ++count;
                ++it;
            }
            ASSERT_EQ(count, n);
        }

        // cbegin cend on const object
        {
            count=0;
            auto it = const_series.cbegin();
            ASSERT_EQ(n, std::size_t(std::distance(it, series.cend())));
            while(it != const_series.cend()) {
                ++count;
                ++it;
            }
            ASSERT_EQ(count, n);
        }
    }
}

TYPED_TEST_P(FrequencySeriesTester, host_conversion)
{
    typedef TypeParam Traits;
    typedef typename Traits::FrequencySeriesType FrequencySeriesType;
    typedef typename FrequencySeriesType::ValueType ValueType;
    typedef FrequencySeries<panda::Cpu, ValueType, std::allocator<ValueType>> HostFrequencySeriesType;
    FourierFrequencyType df = 5.4 * hz;

    // some host based data
    std::size_t n=5;
    HostFrequencySeriesType host_data(df, n, std::allocator<ValueType>());
    ValueType value=0;
    std::transform(host_data.begin(), host_data.end(), host_data.begin(), [&](ValueType const&) { return ++value; });

    Traits traits;
    for(auto& device : this->_system.devices()) {
        // copy host data to target FrequencySeries
        auto allocator = traits.allocator(*device);
        FrequencySeriesType series(host_data, allocator);
        ASSERT_EQ(series.size(), host_data.size());
        // copy back from target to host
        HostFrequencySeriesType host_data_copy(series);
        ASSERT_EQ(host_data_copy.size(), n);
        auto copy_it=host_data_copy.cbegin();
        auto it=host_data.cbegin();
        std::size_t count=0;
        while(it!=host_data.cend()) {
            SCOPED_TRACE("element number: " + std::to_string(count));
            ASSERT_EQ(*it, *copy_it) << host_data[count];
            ++it;
            ++copy_it;
            ++count;
        }
        ASSERT_EQ(df,host_data.frequency_step());
    }
}

REGISTER_TYPED_TEST_CASE_P(FrequencySeriesTester, construct_resize, begin_end, host_conversion);

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
