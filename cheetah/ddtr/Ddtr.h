/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_DDTR_H
#define SKA_CHEETAH_DDTR_DDTR_H

#include "cheetah/ddtr/Config.h"
#include "cheetah/ddtr/detail/CommonTypes.h"
#include "cheetah/ddtr/cpu/Ddtr.h"
#include "cheetah/ddtr/astroaccelerate/Ddtr.h"
#include "panda/ResourceJob.h"
#include "panda/ConfigurableTask.h"
#include "panda/AlgorithmTuple.h"
#include "panda/AggregationBufferFiller.h"
#include <memory>
#include <functional>

namespace ska {
namespace cheetah {
namespace ddtr {

/**
 * @brief device independent interface to DDTR module. A convenience virtual class,
 *        the pipeline should use one of the device specific versions.
 */

template<typename ConfigType, typename NumericalRep>
class Ddtr
{
        typedef CommonTypes<NumericalRep> Common;
        typedef cpu::Ddtr<typename ConfigType::PoolType, NumericalRep> DdtrCpuType;
        typedef astroaccelerate::Ddtr<typename ConfigType::PoolType, NumericalRep> DdtrAstroAccelerateType;
        typedef panda::AlgorithmTuple<DdtrCpuType, DdtrAstroAccelerateType> Implementations;
        enum class AlgorithmEnum {cpu, fpga, astroaccelerate};

    public:
        typedef typename Common::DedispersionHandler DedispersionHandler;
        typedef typename Common::TimeFrequencyType TimeFrequencyType;
        typedef typename Common::DmTrialsType DmTrialsType;

    public:
        Ddtr(ConfigType const& config, DedispersionHandler);
        ~Ddtr();

        /**
         * @brief dedisperses chunk of buffer data to a dm-time chunk.
         *
         * @detail the DedispersionHandler will be called when dedispersion is complete. The method
         *         is delegated device specific implementations.
         *
         * @param[in] input A time-frequency buffer of data to dedisperse.
         */
        void operator()(TimeFrequencyType const& input);

    private:
        DedispersionHandler _dedispersion_handler;
        Implementations _implementations;
        AlgorithmEnum _algo_selected;

};

} // namespace ddtr
} // namespace cheetah
} // namespace ska

#include "cheetah/ddtr/detail/Ddtr.cpp"

#endif // SKA_CHEETAH_DDTR_DDTR_H
