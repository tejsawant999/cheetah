/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_ASTROACCELERATE_DDTR_H
#define SKA_CHEETAH_DDTR_ASTROACCELERATE_DDTR_H

#include "DdtrWorker.h"
#include "cheetah/ddtr/Config.h"
#include "cheetah/ddtr/detail/CommonTypes.h"
#include <panda/DeviceLocal.h>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {

/**
 * @brief an nvidia CUDA gpu algorithm for dedispersion
 */

template<typename PoolType, typename NumericalT>
class Ddtr
{
        typedef ddtr::CommonTypes<NumericalT> Common;
        typedef typename Common::DedispersionHandler DedispersionHandler;
        typedef typename Common::DmTrialsType DmTrialsType;
        typedef typename Common::TimeFrequencyType TimeFrequencyType;
        typedef typename Common::BufferType BufferType;

    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<3, 5, panda::nvidia::giga> ArchitectureCapability;

    public:
        Ddtr(ddtr::Config const& config
            , DedispersionHandler& dm_trials_handler
            , PoolType& pool);
        Ddtr(Ddtr const&) = delete;
        Ddtr(Ddtr&&);

        void operator()(panda::PoolResource<cheetah::Cuda>& gpu, BufferType const& data);

        /**
         * @brief collect data until there is sufficent to process
         * @detail the buffer is filled with dispersed data continously.
         * @param data  data to be processed
         */
         void operator()(TimeFrequencyType const& data);

    protected:
        std::size_t set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const& tf_data, panda::PoolResource<cheetah::Cuda> const& gpu);

    private:
        struct DdtrWorkerFactory;

    private:
        DedispersionHandler& _dm_trials_handler;
        std::unique_ptr<DdtrWorkerFactory> _factory;
        panda::DeviceLocal<Cuda, DdtrWorkerFactory> _workers;
};


} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
#include "detail/Ddtr.cpp"

#endif // SKA_CHEETAH_DDTR_ASTROACCELERATE_DDTR_H
