/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/astroaccelerate/Ddtr.h"


namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {


template<typename PoolType, typename NumericalT>
struct Ddtr<PoolType, NumericalT>::DdtrWorkerFactory
{
    template<typename DeviceType>
    DdtrWorker<NumericalT>* operator()(DeviceType&)
    {
        return new DdtrWorker<NumericalT>();
    }
};

template<typename PoolType, typename NumericalT>
Ddtr<PoolType, NumericalT>::Ddtr(ddtr::Config const&
                                , DedispersionHandler& dm_trials_handler
                                , PoolType&)
    //: _config(config)
    : _dm_trials_handler(dm_trials_handler)
    , _factory(new DdtrWorkerFactory())
    , _workers(*_factory)
{
}

template<typename PoolType, typename NumericalT>
Ddtr<PoolType, NumericalT>::Ddtr(Ddtr&& other)
    : _dm_trials_handler(other._dm_trials_handler)
    , _factory(std::move(other._factory))
    , _workers(std::move(other._workers))
{
}

template<typename PoolType, typename NumericalT>
std::size_t Ddtr<PoolType, NumericalT>::set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const& tf_data, panda::PoolResource<cheetah::Cuda> const& gpu)
{
    return _workers(gpu).set_dedispersion_strategy(min_gpu_memory, tf_data);
}

} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
