/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/astroaccelerate/DdtrWorker.h"


namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {


template<typename NumericalRep>
DdtrWorker<NumericalRep>::DdtrWorker()
{
}

template<typename NumericalRep>
DdtrWorker<NumericalRep>::~DdtrWorker()
{
}
/*
template<typename NumericalRep>
template<typename DmHandler, typename SpHandler>
void DdtrCuda::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , BufferType& agg_buf
                    , DmHandler& dm_handler)
{
    PUSH_NVTX_RANGE("sps_astroaccelerate_DdtrCuda_operator",0);
    PANDA_LOG << "astroaccelerate:: invoked (on device "<< gpu.device_id() << ")";

    if (agg_buf.data_size() < (std::size_t) _dedispersion_strategy->get_maxshift())
    {
        const std::string msg("DdtrCuda: data buffer size < maxshift ");
        PANDA_LOG_ERROR << msg << "(" << agg_buf.data_size() << "<" << _dedispersion_strategy->get_maxshift() << ")";
        throw panda::Error(msg);
    }

    // prepare the DmTime metadata structure

    // run the Cuda kernel
    _ddtr.run_dedispersion_sps(
        gpu.device_id()
        ,tf_data
        ,*_dm_time
        ,sps_cands);

    POP_NVTX_RANGE; //astroaccelerate_run_dedispersion

    dm_handler(dm_time_ptr);
}*/

} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
