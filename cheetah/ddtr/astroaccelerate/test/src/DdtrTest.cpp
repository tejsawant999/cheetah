#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/ddtr/test_utils/DdtrTester.h"
#include <memory>
#include <vector>


namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {
namespace test {

struct AstroAccelerateTraits : public ddtr::test::DdtrTesterTraits<ddtr::astroaccelerate::Ddtr::Architecture,ddtr::astroaccelerate::Ddtr::ArchitectureCapability>
{
    typedef ddtr::test::DdtrTesterTraits<ddtr::astroaccelerate::Ddtr::Architecture, typename ddtr::astroaccelerate::Ddtr::ArchitectureCapability> BaseT;
    typedef typename BaseT::Arch Arch;
    void configure(ddtr::Config& config) override {
        BaseT::configure(config);
        auto& astroaccelerate_config = config.astroaccelerate_config();
        astroaccelerate_config.activate();
    }
};

} // namespace test
} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace ddtr {
namespace test {

typedef ::testing::Types<ddtr::astroaccelerate::test::AstroAccelerateTraits> AstroAccelerateTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, DdtrTester, AstroAccelerateTraitsTypes);

} // namespace test
} // namespace ddtr
} // namespace cheetah
} // namespace ska
