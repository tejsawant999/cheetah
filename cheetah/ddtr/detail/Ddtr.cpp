/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/Ddtr.h"

namespace ska {
namespace cheetah {
namespace ddtr {

template<typename ConfigType, typename NumericalRep>
Ddtr<ConfigType, NumericalRep>::Ddtr(ConfigType const& config, DedispersionHandler handler)
    : _dedispersion_handler(handler)
    , _implementations(std::move(DdtrCpuType(config, _dedispersion_handler, config.pool()))
                      ,std::move(DdtrAstroAccelerateType(config, _dedispersion_handler, config.pool()))
                      )

{
    std::size_t active_count = 0;

    if(config.fpga_algo_config().active())
    {
        PANDA_LOG << "ddtr::fpga algorithm activated";
        ++active_count;
        _algo_selected = AlgorithmEnum::fpga;
    }
    else if (config.cpu_algo_config().active())
    {
        PANDA_LOG << "ddtr::cpu algorithm activated";
        ++active_count;
        _algo_selected = AlgorithmEnum::cpu;
    }
    else if (config.astroaccelerate_algo_config().active())
    {
        PANDA_LOG << "ddtr::astroaccelerate algorithm activated";
        ++active_count;
        _algo_selected = AlgorithmEnum::astroaccelerate;
    }
    else
    {
        PANDA_LOG_WARN << "ddtr:: no algorithm activated";
    }

    if (active_count > 1)
    {
        throw panda::Error("More than one DDTR algorithm is activated");
    }
}

template<typename ConfigType, typename NumericalRep>
Ddtr<ConfigType, NumericalRep>::~Ddtr()
{
}

template<typename ConfigType, typename NumericalRep>
void Ddtr<ConfigType, NumericalRep>::operator()(TimeFrequencyType const& tf_data)
{
    switch (_algo_selected)
    {
        case AlgorithmEnum::fpga:
           throw panda::Error("FPGA algorithm not implemented.");
        case AlgorithmEnum::cpu:
           _implementations.template get<Cpu>()(tf_data);
           break;
        default:
           throw panda::Error("No algorithm available.");
    }
}

} // namespace ddtr
} // namespace cheetah
} // namespace ska
