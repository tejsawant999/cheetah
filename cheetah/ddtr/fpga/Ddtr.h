/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_FPGA_DDTR_H
#define SKA_CHEETAH_DDTR_FPGA_DDTR_H

#include "cheetah/ddtr/fpga/DdtrWorker.h"
#include "cheetah/ddtr/fpga/Config.h"
#include "cheetah/ddtr/Config.h"
#include "cheetah/ddtr/detail/CommonTypes.h"
#include "cheetah/utils/Architectures.h"
#include "panda/DeviceLocal.h"
#include "panda/arch/altera/Fpga.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace fpga {

/**
 * @brief
 *      Interface for the Altera fpga algorithm
 */

template<typename PoolType, typename NumericalT>
class Ddtr
{
    public:
        typedef typename cheetah::Fpga Architecture;

    private:
        typedef CommonTypes<NumericalT> CommonTraits;
        typedef typename CommonTraits::DmTrialsType DmTrialsType;
        typedef typename CommonTraits::BufferFillerType BufferFillerType;
        typedef typename CommonTraits::TimeFrequencyType TimeFrequencyType;
        typedef typename CommonTraits::BufferType BufferType;
        typedef typename CommonTraits::DedispersionHandler DedispersionHandler;

        typedef std::vector<ddtr::Config::Dm> DmListType;
        typedef ddtr::Config::Dm Dm;
        typedef typename TimeFrequencyType::FrequencyType FrequencyType;
        typedef typename TimeFrequencyType::TimeType TimeType;

    public:
        Ddtr(ddtr::Config const&, DedispersionHandler const&, PoolType& pool);
        Ddtr(Ddtr&&);
        Ddtr(Ddtr const&) = delete;
        ~Ddtr();

        template<typename TimeFrequencyT>
        void operator()(TimeFrequencyT const& input);
        void operator()(panda::PoolResource<Architecture>& device, BufferType const& data);

    protected:
        void init(TimeFrequencyType const&);

    private:
        struct WorkerFactory {
            public:
                WorkerFactory(ddtr::Config const&);

                DdtrWorker<BufferType, DmTrialsType>* operator()(panda::PoolResource<Architecture> const& device) {
                    return new DdtrWorker<BufferType, DmTrialsType>(_config, device);
                }


            private:
                ddtr::Config const& _config;
        };

    private:
        bool _first_call;
        DedispersionHandler _dedispersion_handler;
        PoolType& _pool;
        panda::DeviceLocal<panda::PoolResource<Architecture>, WorkerFactory> _workers;
        std::unique_ptr<BufferFillerType> _agg_buffer_filler_ptr;
        std::size_t _max_delay;
        std::size_t _dedispersion_samples;
        ddtr::Config const& _config;
        std::vector<double> _dm_factors;
        std::size_t _dev_memory;
};


} // namespace fpga
} // namespace ddtr
} // namespace cheetah
} // namespace ska
#include "cheetah/ddtr/fpga/detail/Ddtr.cpp"
#endif // SKA_CHEETAH_DDTR_FPGA_DDTR_H
