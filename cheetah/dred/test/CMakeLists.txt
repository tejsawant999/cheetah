include_directories(${GTEST_INCLUDE_DIR})

link_directories(${GTEST_LIBRARY_DIR})

set(
   gtest_dred_src
    src/gtest_dred.cpp
)
add_executable(gtest_dred ${gtest_dred_src} )
target_link_libraries(gtest_dred ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_dred gtest_dred --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
