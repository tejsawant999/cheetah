set(module_opencl_lib_src_cpu
    src/Config.cpp
    src/Fdas.cpp
    PARENT_SCOPE
)

TEST_UTILS()

add_subdirectory(test)
