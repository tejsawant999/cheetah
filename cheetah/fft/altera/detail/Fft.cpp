/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fft/altera/Fft.h"

namespace ska {
namespace cheetah {
namespace fft {
namespace altera {

#ifdef ENABLE_OPENCL
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::TimeSeries<cheetah::Fpga,T,InputAlloc> const& input,
    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output)
{
    //update the size of the output buffer to match output transform size, only half spectrum considered
    output.resize(input.size()/2 + 1);
    _workers(fpga)(input, output);
    //Calculate the new frequency step that the output will have
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
}
/*
 * commented FFT types for future scope: C2R, C2C_fwd, C2C_inv
 */
/*
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, InputAlloc> const& input,
    data::TimeSeries<cheetah::Fpga,T,OutputAlloc>& output)
{
    //update the size of the output buffer to match output transform size
    output.resize(2*(input.size() - 1));
    //Calculate the new sampling time that the output will have
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
}
*/

/*
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::TimeSeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output)
{
    //update the size of the output buffer to match output transform size
    output.resize(input.size());
    //Calculate the new frequency step that the output will have
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
}
*/

/*
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::FrequencySeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
    data::TimeSeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output)
{
    //update the size of the output buffer to match output transform size
    output.resize(input.size());
    //Calculate the new sampling time that the output will have
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
}
*/
#endif // ENABLE_OPENCL

} // namespace altera
} // namespace fft
} // namespace cheetah
} // namespace ska
