#include "cheetah/fft/test_utils/FftTester.h"
#include "cheetah/fft/altera/Fft.h"

namespace ska {
namespace cheetah {
namespace fft {
namespace altera {
namespace test {

template<typename NumericalT>
struct AlteraTraits
    : public fft::test::FftTesterTraits<fft::altera::Fft, NumericalT>
{
    typedef fft::test::FftTesterTraits<fft::altera::Fft, NumericalT> BaseT;
    typedef typename BaseT::DeviceType DeviceType;

    static const std::size_t fft_trial_length = 8388608;
};

} // namespace test
} // namespace altera
} // namespace fft
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace fft {
namespace test {

//typedef ::testing::Types<altera::test::AlteraTraits<uint8_t>, altera::test::AlteraTraits<uint16_t>, altera::test::AlteraTraits<float>> AlteraTraitsTypes;
typedef ::testing::Types<altera::test::AlteraTraits<float>> AlteraTraitsTypes;
#ifdef ENABLE_OPENCL
INSTANTIATE_TYPED_TEST_CASE_P(Fpga, FftTester, AlteraTraitsTypes);
#endif // ENABLE_OPENCL

} // namespace test
} // namespace fft
} // namespace cheetah
} // namespace ska
