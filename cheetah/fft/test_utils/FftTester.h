/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FFT_TEST_FFTTESTER_H
#define SKA_CHEETAH_FFT_TEST_FFTTESTER_H

#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "cheetah/data/Units.h"
#include "cheetah/fft/Config.h"
#include "cheetah/fft/Fft.h"
#include "panda/AlgorithmInfo.h"

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace fft {
namespace test {

/**
 * @brief
 * Generic functional test for the FftTester algorithm
 *
 * @details
 * The tests generated will correspond to the process() methods on the interface. If the process method
 * does not exist then the corresponding test will not be made and a message will instead be produced
 * saying the test has been deactivated.
 *
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requirements it needs to run. There are a number of overrides that can be specified
 * concerning the lenght of data to trial, the tolerance for results, and data type allocator
 * construction. See the method help for details.
 *
 * e.g.
 * @code
 * struct CudaTraits : public FftTesterTraits<MyAlgorthmType>
 * {
 *   typedef test::FftTesterTraits<MyAlgorithmType> BaseT;
 *   typedef Fft::Architecture Arch;
 *   typedef typename BaseT::DeviceType DeviceType;
 *
 *   // add any overrides here e.g.
     // static const std::size_t fft_trial_length = 2048;
 *
 * };
 * @endcode
 *
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_CASE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<CudaTraits, AlteraTraits, CpuTraits> MyTypes;
 * INSTANTIATE_TYPED_TEST_CASE_P(MyAlgo, FftTester, MyTypes);
 * @endcode
 *  n.b. the INSTANTIATE_TYPED_TEST_CASE_P must be in the same namespace as this class
 *
 */

template<typename Algorithm, typename NumericalT>
struct FftTesterTraits
    : public utils::test::AlgorithmTesterTraits<typename Algorithm::Architecture, typename panda::AlgorithmInfo<Algorithm>::ArchitectureCapability>
{
    public:
        typedef Algorithm Algo;
        typedef typename Algorithm::Architecture Arch;
        typedef typename panda::AlgorithmInfo<Algorithm>::ArchitectureCapability ArchCap;
        typedef utils::test::AlgorithmTesterTraits<Arch, ArchCap> BaseT;
        typedef typename data::ComplexTypeTraits<Arch,NumericalT>::type ComplexT;
        typedef typename BaseT::DeviceType DeviceType;
        typedef NumericalT NumericalRep;

    public:
        FftTesterTraits();
        virtual ~FftTesterTraits() {}

        /**
         * @brief defines accuracy requirement for when the input data is compared
         *        with the Fourier transform followed by an inverse fourier transform
         * @details override this method to customise the expected tolerance
         */
        static double accuracy() { return 1.0e-10; }

        /**
         * @brief provides a default allocator contruction for the DataType provided
         * @details override to specialise
         */
        template<typename DataType>
        typename DataType::Allocator allocator(panda::PoolResource<Arch>& device) {
            return typename DataType::Allocator(device);
        }

    public:
        // do not override method calls below this comment
        fft::Fft& api();


    public:
        /**
         * @brief FFT trial length (size of input data)
         * @details Override to adjust
         */
        static const std::size_t fft_trial_length = 1024;

    private:
        fft::Config _config;
        fft::Fft _api;

};


template <typename TestTraits>
class FftTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        FftTester();
        ~FftTester();

    private:
};

TYPED_TEST_CASE_P(FftTester);

} // namespace test
} // namespace fft
} // namespace cheetah
} // namespace ska

#include "cheetah/fft/test_utils/detail/FftTester.cpp"


#endif // SKA_CHEETAH_FFT_TEST_FFTTESTER_H
