/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CONFIG_H
#define SKA_CHEETAH_FLDO_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/fldo/cpu/Config.h"
#include "cheetah/fldo/cuda/Config.h"
#include "cheetah/data/Units.h"
#include "cheetah/pipeline/ScanConfig.h"
#include "panda/ProcessingEngine.h"
#include "panda/ResourcePool.h"
#include "panda/arch/nvidia/Nvidia.h"
#include "panda/PoolSelector.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace fldo {

/**
 * @brief
 *    Configuration details for the fldo module
 *
 * @details
 *    Contains common algorithm independent configuration options
 *    and access to the config objects of specific algorithms
 */

class Config : public utils::Config
{
    public:
        typedef data::TimeType TimeType;

    public:
        Config(pipeline::ScanConfig const& );
        ~Config();

        /**
         * @brief return the cpu algorithm specific options
         */
        fldo::cpu::Config const& cpu_algo_config() const;
        fldo::cpu::Config& cpu_algo_config();

        /**
         * @brief return the CUDA algorithm specific options
         */
        fldo::cuda::Config const& cuda_algo_config() const;
        fldo::cuda::Config& cuda_algo_config();

        /**
         * @brief return the common input parameters configuration
         */
        pipeline::ScanConfig const& pipeline_config() const;

        /**
         * @brief return the number of phase bins
         */
        size_t const& phases() const;

        /**
         * @brief return the number of sub-integrations used in
         * the sum-up data
         */
        size_t const& nsubints() const;

        /**
         * @brief return the number of frequency sub-bands
         * summed up
         */
        size_t const& nsubbands() const;

        /**
         * @brief Configure the maximum number of phases used in folding
         */
        /**
         * void phases(size_t n)
         *
         * @param[in]  n     maximum number of phases used in folding
         *
         */
        void phases(size_t n);

        /**
         * @brief Configure the number of subbands
         */
        /**
         * void nsubbands(size_t n)
         *
         * @param[in]  n     the number of subbands
         */
        void nsubbands(size_t n);

        /**
         * @brief Configure the number of subints
         */
        /**
         * void nsubints(size_t n)
         *
         * @param[in]  n     the number of subints
         */
        void nsubints(size_t n);

        /**
         * fldo_input_check(fldo::Config const& config)
         * @brief
         * verify input config parameter are inside the bounds defined in
         * CommonDefs.h.
         *
         * \return < 0 if error
         */
        int fldo_input_check(const fldo::Config & ) ;


    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        pipeline::ScanConfig const& _scan_config;
        size_t _nsubints;               /**< the number of sub-integrations (default 64)     */
        size_t _nsubbands;              /**< the number of frequency sub-bands (default 64)  */
        size_t _phases;                 /**< the number of phase bins (default 128)          */
        fldo::cpu::Config _cpu_config;
        fldo::cuda::Config _cuda_config;
};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_FLDO_CONFIG_H
