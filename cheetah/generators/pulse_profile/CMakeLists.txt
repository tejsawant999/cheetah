# -- add profile specific cmake macros
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/profile_tools.cmake)

ADD_PROFILE("B0011+47")
PROFILE_INIT()

set(module_pulse_profile_lib_src_cpu
    src/AsciiProfileFile.cpp
    src/ProfileManager.cpp
    src/PulsarProfile.cpp
    src/PulsarProfileId.cpp
    src/PulsarProfileConfig.cpp
    ${lib_src_cpu}
    PARENT_SCOPE
)

TEST_UTILS()

add_subdirectory(test)
