include_directories(${GTEST_INCLUDE_DIR})

set(
    gtest_hrms_src_cpu
    src/gtest_hrms.cpp
)

set(
    gtest_hrms_src_cuda
)

if(ENABLE_CUDA)
    cuda_add_executable(gtest_hrms ${gtest_hrms_src_cuda} ${gtest_hrms_src_cpu})
else(ENABLE_CUDA)
    add_executable(gtest_hrms ${gtest_hrms_src_cpu})
endif(ENABLE_CUDA)

target_link_libraries(gtest_hrms ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_hrms gtest_hrms)
