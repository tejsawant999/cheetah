/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_ACCELERATIONSEARCH_H
#define SKA_CHEETAH_PIPELINE_ACCELERATIONSEARCH_H
#include "cheetah/pipeline/SinglePulseFactory.h"
#include "cheetah/pipeline/Dedispersion.h"
#include "cheetah/pipeline/AccelerationSearchAlgoConfig.h"
#include "cheetah/psbc/Psbc.h"
#include "cheetah/sift/Sift.h"
#include "cheetah/fldo/Fldo.h"
#include <panda/DataSwitch.h>
#include <panda/TypeTraits.h>
#include <memory>

namespace ska {
namespace cheetah {
namespace pipeline {

/**
 * @brief
 *    The acceleration search pipeline
 *
 */

template<typename NumericalT>
struct DefaultAccelerationSearchTraits
{

    // traits must provide a factory to produce a suitable dedispersion pipeline object
    template<typename DmHandlerT>
    static inline
    Dedispersion<NumericalT>* create_dedispersion_pipeline( CheetahConfig<NumericalT> const& cheetah_config
                                                          , BeamConfig<NumericalT> const& beam_config
                                                          , DmHandlerT dm_handler
                                                          )
    {
        SinglePulseFactory<NumericalT> sp_factory(cheetah_config);
        return sp_factory.create(beam_config, dm_handler);
    }

};

template<typename NumericalT, typename AccelerationSearchTraitsT>
class AccelerationSearch : public PipelineHandler<NumericalT>
{
    private:
        typedef PipelineHandler<NumericalT> BaseT;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;
        typedef typename panda::is_pointer_wrapper<typename Dedispersion<NumericalT>::DmTrialType>::type DmTrialsType;

    public:
        AccelerationSearch(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const&);
        virtual ~AccelerationSearch();

        /**
         * @brief called whenever data is available for processing
         */
        void operator()(TimeFrequencyType&) override;

        /**
         * @brief reset the pipeline ready for the next observation run
         */
        void do_folding();

    private:
        struct FldoHandler {
            FldoHandler(AccelerationSearch&);
            void operator()(std::shared_ptr<data::Ocld>) const;

            private:
                DataExport<NumericalT>& _out;
        };

        struct SiftHandler {
            public:
                SiftHandler(AccelerationSearch&);
                void operator()(std::shared_ptr<data::Scl>) const;
            private:
                AccelerationSearch& _pipeline;
        };

    private:
        CheetahConfig<NumericalT> const& _config;
        FldoHandler _fldo_handler;
        fldo::Fldo<FldoHandler> _fldo;
        SiftHandler _sift_handler;
        sift::Sift<SiftHandler> _sift;
        typedef typename std::remove_pointer<decltype(std::declval<AccelerationSearchTraitsT>().create_acceleration_search_algo(std::declval<AccelerationSearchAlgoConfig const&>(), _sift))>::type AccelerationSearchAlgoType;
        std::unique_ptr<AccelerationSearchAlgoType> _acceleration_search;
        psbc::Psbc<typename decltype(_acceleration_search)::element_type> _psbc;
        ska::panda::DataSwitch<DmTrialsType> _dm_switch; // need to be able to switch pipelines on/off at runtime

        std::unique_ptr<Dedispersion<NumericalT>> _dedisperser;
        std::vector<std::shared_ptr<TimeFrequencyType>> _tf_data;
        data::Scl _scl; // output from the acceleration search pipeline

    public: // should be protected eventually
        AccelerationSearchAlgoType const& acceleration_search_pipeline() const; // returns the acceleration search object with templated type AccelerationSearchAlgoType
        Dedispersion<NumericalT> const& dedispersion_pipeline() const; // returns the dedispersion pipeline object

};

} // namespace pipeline
} // namespace cheetah
} // namespace ska
#include "detail/AccelerationSearch.cpp"

#endif // SKA_CHEETAH_PIPELINE_ACCELERATIONSEARCH_H
