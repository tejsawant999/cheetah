/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/CheetahConfig.h"
#include "panda/Log.h"
#include <iostream>

namespace ska {
namespace cheetah {
namespace pipeline {


template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::FldoHandler::FldoHandler(AccelerationSearch& pipeline)
    : _out(pipeline.out())
{
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
void AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::FldoHandler::operator()(std::shared_ptr<data::Ocld> data) const
{
    _out.send(panda::ChannelId("ocld"), data);
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::SiftHandler::SiftHandler(AccelerationSearch& pipeline)
    : _pipeline(pipeline)
{
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
void AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::SiftHandler::operator()(std::shared_ptr<data::Scl> scl) const
{
    _pipeline._scl += *scl;
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::AccelerationSearch(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const& beam_config)
    : BaseT(config, beam_config)
    , _config(config)
    , _fldo_handler(*this)
    , _fldo(config.fldo_config(), _fldo_handler)
    , _sift_handler(*this)
    , _sift(config.sift_config(), _sift_handler)
    , _acceleration_search(AccelerationSearchTraitsT::create_acceleration_search_algo(config.acceleration_search_config(), _sift))
    , _psbc(config.psbc_config(), *_acceleration_search)
    , _dm_switch(config.switch_config())
    , _dedisperser(AccelerationSearchTraitsT::create_dedispersion_pipeline(config, beam_config,
        [this](std::shared_ptr<DmTrialsType> data) {
            _dm_switch.send(panda::ChannelId("acc_search"), data);
        }
        ))
{
    _dm_switch.template add<DmTrialsType>(panda::ChannelId("acc_search"),
        [this](DmTrialsType& data) {
            // acc search pipeline
            // call the psbc operator()
            _psbc(data.shared_from_this());
        });
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::~AccelerationSearch()
{
    _config.pool_manager().wait();
    PANDA_LOG_DEBUG << "AccelerationSearch<NumericalT, typename AccelerationSearchTraitsT>::~AccelerationSearch()";
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
void AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::do_folding()
{
    _fldo(_tf_data, _scl);
    _tf_data.clear();
    _scl.clear();
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
void AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::operator()(TimeFrequencyType& chunk)
{
    _tf_data.push_back(chunk.shared_from_this());
    (*_dedisperser)(chunk);
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
typename AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::AccelerationSearchAlgoType const& AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::acceleration_search_pipeline() const
{
    return *_acceleration_search;
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
Dedispersion<NumericalT> const& AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::dedispersion_pipeline() const
{
    return *_dedisperser;
}

} // namespace pipeline
} // namespace cheetah
} // namespace ska
