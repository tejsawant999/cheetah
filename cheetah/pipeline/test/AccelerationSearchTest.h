/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_TEST_ACCELERATIONSEARCHPIPELINE_H
#define SKA_CHEETAH_PIPELINE_TEST_ACCELERATIONSEARCHPIPELINE_H

#include <gtest/gtest.h>
#include "cheetah/data/DmTime.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/pipeline/Dedispersion.h"
#include "cheetah/pipeline/AccelerationSearch.h"

namespace ska {
namespace cheetah {
namespace pipeline {
namespace test {

template<typename NumericalT>
struct TestAccelerationSearchTraits
{

    public:
        typedef typename cheetah::data::DmTrials<cheetah::Cpu,float> DmTrialsType;
        typedef typename cheetah::data::DmTime<DmTrialsType> DmTimeType;
        typedef typename cheetah::pipeline::Dedispersion<NumericalT> BaseT;

        struct TestHandler : Dedispersion<NumericalT>
        {

            typedef typename Dedispersion<NumericalT>::DmHandler DedispersionHandler;

            TestHandler(CheetahConfig<NumericalT> const&, BeamConfig<NumericalT> const&, DedispersionHandler);

            void operator()(typename BaseT::TimeFrequencyType&) override;

            bool wait_dedispersion_handler_called() const;

            private:
                mutable std::condition_variable _dedispersion_wait;
                mutable std::mutex _dedispersion_data_mutex;
                std::vector<std::shared_ptr<typename BaseT::TimeFrequencyType>> _received_tf_data;
                bool _dm_handler_called = false;

        };

        struct AccelerationTestHandler
        {

            void operator()(std::shared_ptr<DmTimeType> const&);

            bool wait_sift_handler_called() const;

            private:
                mutable std::condition_variable _acceleration_wait;
                mutable std::mutex _acceleration_data_mutex;
                std::vector<std::shared_ptr<DmTimeType>> _received_data;
                bool _sift_handler_called = false;

        };

        template<typename SiftHandlerT>
        static
        AccelerationTestHandler* create_acceleration_search_algo(AccelerationSearchAlgoConfig const&, SiftHandlerT&);

        template<typename DmHandlerT>
        static
        TestHandler* create_dedispersion_pipeline(CheetahConfig<NumericalT> const&, BeamConfig<NumericalT> const&, DmHandlerT);

};

class AccelerationSearchTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        AccelerationSearchTest();
        ~AccelerationSearchTest();

    private:
};

} // namespace test
} // namespace pipeline
} // namespace cheetah
} // namespace ska

#endif //SKA_CHEETAH_PIPELINE_TEST_ACCELERATIONSEARCHPIPELINE_H
