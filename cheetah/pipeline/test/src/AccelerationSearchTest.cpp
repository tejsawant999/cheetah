/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/test/AccelerationSearchTest.h"
#include "cheetah/pipeline/AccelerationSearch.h"
#include "cheetah/pipeline/CheetahConfig.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/ddtr/DedispersionConfig.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/GaussianNoiseConfig.h"
#include <memory>
#include <chrono>
#include <thread>

namespace ska {
namespace cheetah {
namespace pipeline {
namespace test {

AccelerationSearchTest::AccelerationSearchTest()
    : ::testing::Test()
{
}

AccelerationSearchTest::~AccelerationSearchTest()
{
}

void AccelerationSearchTest::SetUp()
{
}

void AccelerationSearchTest::TearDown()
{
}

template<typename NumericalT>
TestAccelerationSearchTraits<NumericalT>::TestHandler::TestHandler(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const& beam_config, DedispersionHandler dm_handler)
    : BaseT(config, beam_config, dm_handler)
{
}

template<typename NumericalT>
void TestAccelerationSearchTraits<NumericalT>::TestHandler::operator()(typename BaseT::TimeFrequencyType& data)
{
    std::lock_guard<std::mutex> lock(_dedispersion_data_mutex);
    _received_tf_data.push_back(data.shared_from_this());
    std::shared_ptr<data::DmTrialsMetadata> dm_trial_metadata = std::make_shared<data::DmTrialsMetadata>(typename data::DmTrialsMetadata::TimeType(data.sample_interval()), 1);
    dm_trial_metadata->emplace_back(10 * data::parsecs_per_cube_cm);
    std::shared_ptr<DmTrialsType> dm_trials_ptr = DmTrialsType::make_shared(dm_trial_metadata, data.start_time());
    this->_dm_handler(dm_trials_ptr);
    _dedispersion_wait.notify_all();
    _dm_handler_called = true;
}

template<typename NumericalT>
bool TestAccelerationSearchTraits<NumericalT>::TestHandler::wait_dedispersion_handler_called() const
{
    std::unique_lock<std::mutex> lock(_dedispersion_data_mutex);
    _dedispersion_wait.wait_for(lock, std::chrono::seconds(5), [this] { return _dm_handler_called; });
    return _dm_handler_called;
}

template<typename NumericalT>
void TestAccelerationSearchTraits<NumericalT>::AccelerationTestHandler::operator()(std::shared_ptr<TestAccelerationSearchTraits<NumericalT>::DmTimeType> const& data)
{
    std::lock_guard<std::mutex> lock(_acceleration_data_mutex);
    _received_data.push_back(data);
    _acceleration_wait.notify_all();
    _sift_handler_called = true;
}

template<typename NumericalT>
bool TestAccelerationSearchTraits<NumericalT>::AccelerationTestHandler::wait_sift_handler_called() const
{
    std::unique_lock<std::mutex> lock(_acceleration_data_mutex);
    _acceleration_wait.wait_for(lock, std::chrono::seconds(5), [this] { return _sift_handler_called; });
    return _sift_handler_called;
}

template<typename NumericalT>
template<typename SiftHandlerT>
typename TestAccelerationSearchTraits<NumericalT>::AccelerationTestHandler* TestAccelerationSearchTraits<NumericalT>::create_acceleration_search_algo(AccelerationSearchAlgoConfig const&, SiftHandlerT&)
{
    return new TestAccelerationSearchTraits<NumericalT>::AccelerationTestHandler;
}

template<typename NumericalT>
template<typename DmHandlerT>
typename TestAccelerationSearchTraits<NumericalT>::TestHandler* TestAccelerationSearchTraits<NumericalT>::create_dedispersion_pipeline(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const& beam_config, DmHandlerT dm_handler)
{
    return new TestAccelerationSearchTraits<NumericalT>::TestHandler(config, beam_config, dm_handler);
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
struct AccelerationSearchTests
{
    static void run()
    {
        // Search ~600 seconds of data at standard resoluton
        typedef data::TimeFrequency<Cpu, NumericalT> TimeFrequencyType;
        typedef typename TimeFrequencyType::DataType DataType;

        // Configure components
        CheetahConfig<NumericalT> config;
        BeamConfig<NumericalT> beam_config;

        // Configure high-density low DMs
        ddtr::DedispersionConfig dd_config_low;
        config.sps_config().dedispersion_config(dd_config_low);
        dd_config_low.dm_start(ddtr::DedispersionConfig::Dm(0.0 * data::parsecs_per_cube_cm));
        dd_config_low.dm_end(ddtr::DedispersionConfig::Dm(100.0 * data::parsecs_per_cube_cm));
        dd_config_low.dm_step(ddtr::DedispersionConfig::Dm(0.1 * data::parsecs_per_cube_cm));

/*
        // Configure low-density high DMs
        ddtr::DedispersionConfig dd_config_high;
        config.sps_config().dedispersion_config(dd_config_high);
        dd_config_high.dm_start(ddtr::DedispersionConfig::Dm(200.0 * data::parsecs_per_cube_cm));
        dd_config_high.dm_end(ddtr::DedispersionConfig::Dm(300.0 * data::parsecs_per_cube_cm));
        dd_config_high.dm_step(ddtr::DedispersionConfig::Dm(1.0 * data::parsecs_per_cube_cm));
*/

        // Set sps priority
        config.sps_config().set_priority(0);

        // Set the size of the dedispersion buffer
        config.sps_config().set_dedispersion_samples(1<<18);

        // Set the psbc buffer size via the dump_time in the psbc config, i.e. psbc will flush its buffer after this amount of time
        config.psbc_config().dump_time(0.0001 * data::seconds);

        typedef AccelerationSearch<NumericalT, TestAccelerationSearchTraits<NumericalT>> AccelerationSearchType;
        AccelerationSearchType pipeline(config, beam_config);

        // Set up noise parameters for data to be passed through the pipeline
        generators::GaussianNoiseConfig noise_config;
        generators::GaussianNoise<DataType> noise(noise_config);
        noise_config.mean(96.0);
        noise_config.std_deviation(10.0);

        // Start epoch
        typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(50000.0));

        double tsamp_us = 64.0;
        double f_low = 1.2;
        double f_high = 1.8;
        std::size_t total_nsamps = std::size_t(1.0 / (tsamp_us * 1e-6));
        data::DimensionSize<data::Time> number_of_samples(1000);
        data::DimensionSize<data::Frequency> number_of_channels(1024);
        std::size_t loop_count = total_nsamps/number_of_samples + 10;
        for (std::size_t ii=0; ii<loop_count; ++ii)
        {
            auto tf = std::make_shared<TimeFrequencyType>(number_of_samples, number_of_channels);
            auto f1 =  typename TimeFrequencyType::FrequencyType(f_high * boost::units::si::giga * boost::units::si::hertz);
            auto f2 =  typename TimeFrequencyType::FrequencyType(f_low * boost::units::si::giga * boost::units::si::hertz);
            auto delta = (f2 - f1)/ (double)number_of_channels;
            tf->set_channel_frequencies_const_width( f1, delta );
            tf->sample_interval(typename TimeFrequencyType::TimeType(tsamp_us * boost::units::si::micro * data::seconds));
            tf->start_time(epoch);
            epoch += std::chrono::duration<double>(tf->sample_interval().value()*number_of_samples);
            noise.next(*tf);
            pipeline(*tf);
        }

        typename TestAccelerationSearchTraits<NumericalT>::AccelerationTestHandler const& acceleration_search_pipeline_object = pipeline.acceleration_search_pipeline();
        typename TestAccelerationSearchTraits<NumericalT>::TestHandler const& dedispersion_pipeline_object = static_cast<typename TestAccelerationSearchTraits<NumericalT>::TestHandler const&>(pipeline.dedispersion_pipeline());
        ASSERT_TRUE(acceleration_search_pipeline_object.wait_sift_handler_called());
        ASSERT_TRUE(dedispersion_pipeline_object.wait_dedispersion_handler_called());
    }
};

TEST_F(AccelerationSearchTest, simple_run_test_uint8)
{
    AccelerationSearchTests<uint8_t, TestAccelerationSearchTraits<uint8_t>>::run();
}

TEST_F(AccelerationSearchTest, simple_run_test_uint16)
{
    AccelerationSearchTests<uint16_t, TestAccelerationSearchTraits<uint16_t>>::run();
}

} // namespace test
} // namespace pipeline
} // namespace cheetah
} // namespace ska
