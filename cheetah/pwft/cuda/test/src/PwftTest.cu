#include "cheetah/pwft/cuda/Pwft.cuh"
#include "cheetah/pwft/test_utils/PwftTester.h"

namespace ska {
namespace cheetah {
namespace pwft {
namespace cuda {
namespace test {

struct CudaTraits
    : public pwft::test::PwftTesterTraits<pwft::cuda::Pwft::Architecture, typename pwft::cuda::Pwft::ArchitectureCapability>
{
    typedef pwft::test::PwftTesterTraits<pwft::cuda::Pwft::Architecture, typename pwft::cuda::Pwft::ArchitectureCapability> BaseT;
    typedef Pwft::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace pwft
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace pwft {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, PwftTester, CudaTraitsTypes);

} // namespace test
} // namespace pwft
} // namespace cheetah
} // namespace ska