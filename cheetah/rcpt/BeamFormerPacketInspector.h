/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_RCPT_BEAMFORMERPACKETINSPECTOR_H
#define SKA_CHEETAH_RCPT_BEAMFORMERPACKETINSPECTOR_H

#include "cheetah/rcpt/BeamFormerPacket.h"

namespace ska {
namespace cheetah {
namespace rcpt {

/**
 * @brief
 *      BeamFormerPacket inspection and data extraction
 */
class BeamFormerPacketInspector
{
    public:
        typedef BeamFormerPacketMid Packet;

    public:
        BeamFormerPacketInspector(Packet const& packet);
        ~BeamFormerPacketInspector();

        /**
         * @brief return the sequence number embedded in the packet
         */
        uint64_t sequence_number() const;

        /**
         * @brief return true if no further processing is required on this packet
         */
        bool ignore() const;

        inline Packet const& packet() const;

    private:
        Packet const& _packet;
};


} // namespace rcpt
} // namespace cheetah
} // namespace ska
#include "cheetah/rcpt/detail/BeamFormerPacketInspector.cpp"

#endif // SKA_CHEETAH_RCPT_BEAMFORMERPACKETINSPECTOR_H
