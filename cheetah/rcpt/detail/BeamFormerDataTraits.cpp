/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt/BeamFormerDataTraits.h"
#include <panda/Log.h>
#include <algorithm>


namespace ska {
namespace cheetah {
namespace rcpt {

template<typename ContextType>
void BeamFormerDataTraits::deserialise_packet(ContextType& context, PacketInspector const& packet)
{
    // case I: packet > chunk
    // [-- Packet 0 --][-- Packet 1 --][-- Packet 2 --]
    // [Chunk 0][Chunk 1][Chunk 2]
    // chunk_len * chunk_number = packet_num * packet_data
    // chunk_len * chunk_number = packet_num * (chunk_len + delta) + offset
    // offset = chunk_len * chunk_number - packet_num * (chunk_len + delta)
    //        = chunk_len(chunk_number - packet_num) - packet_num * delta
    //
    unsigned offset = context.offset(); // * (context.chunk().number_of_channels() * context.chunk().number_of_samples())  - (context.sequence_number() * context.data().packet_size());
    auto packet_it=packet.packet().begin() + context.packet_offset();
    auto it=context.chunk().begin() + offset; //(BeamFormerDataTraits::sequence_number(packet) - context.start()) + offset;
    PANDA_LOG_DEBUG << " --- deserialise_packet: sequence_number=" << context.sequence_number(packet)
                    << " " << context;
    std::for_each(packet_it, packet_it + context.size(), [&](Sample const& sample) { *it=sample.xx(); ++it; } );
    //do_deserialise_packet(context, packet.packet(), 0U, std::move(it));
}

template<typename ContextType>
void BeamFormerDataTraits::process_missing_slice(ContextType& context)
{
    unsigned offset = context.offset();
    auto it=context.chunk().begin() + offset;
    PANDA_LOG_DEBUG << "processing missing packet: data=" << (void*)&*it << context;
    for(std::size_t i=0; i < context.size() ; ++i) {
        *it = 99;
        assert(it!=context.chunk().end());
        ++it;
    }
}

template<typename ResizeContextType>
void BeamFormerDataTraits::resize_chunk(ResizeContextType& context)
{
    PANDA_LOG_DEBUG << "resizing data: " << context;
    // drops any incomplete spectra
    context.chunk().resize(data::DimensionSize<data::Time>(context.size()/context.chunk().number_of_channels()));
}

constexpr uint64_t BeamFormerDataTraits::max_sequence_number()
{
    return PacketInspector::Packet::max_sequence_number();
}

} // namespace rcpt
} // namespace cheetah
} // namespace ska
