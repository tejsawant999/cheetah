/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt/test/BeamFormerPacketInspectorTest.h"
#include "cheetah/rcpt/BeamFormerPacketInspector.h"
#include "cheetah/data/TimeFrequency.h"
#include <memory>
#include <memory>

namespace ska {
namespace cheetah {
namespace rcpt {
namespace test {


BeamFormerPacketInspectorTest::BeamFormerPacketInspectorTest()
    : ::testing::Test()
{
}

BeamFormerPacketInspectorTest::~BeamFormerPacketInspectorTest()
{
}

void BeamFormerPacketInspectorTest::SetUp()
{
}

void BeamFormerPacketInspectorTest::TearDown()
{
}

TEST_F(BeamFormerPacketInspectorTest, test_ignore)
{
    // we want stoke I packets
    BeamFormerPacketInspector::Packet packet;
    packet.packet_type(PacketType::StokesI);
    {
        BeamFormerPacketInspector inspector(packet);
        ASSERT_FALSE(inspector.ignore());
    }
    packet.packet_type(PacketType::StokesQ);
    {
        BeamFormerPacketInspector inspector(packet);
        ASSERT_TRUE(inspector.ignore());
    }
    packet.packet_type(PacketType::StokesRe);
    {
        BeamFormerPacketInspector inspector(packet);
        ASSERT_TRUE(inspector.ignore());
    }
    packet.packet_type(PacketType::StokesIm);
    {
        BeamFormerPacketInspector inspector(packet);
        ASSERT_TRUE(inspector.ignore());
    }
    packet.packet_type(PacketType::AntennaCount);
    {
        BeamFormerPacketInspector inspector(packet);
        ASSERT_TRUE(inspector.ignore());
    }
}

TEST_F(BeamFormerPacketInspectorTest, test_sequence_number)
{
    BeamFormerPacketInspector::Packet packet;
    packet.packet_count(0);
    {
        BeamFormerPacketInspector inspector(packet);
        ASSERT_EQ(0U, inspector.sequence_number());
    }
    packet.packet_count(55);
    {
        BeamFormerPacketInspector inspector(packet);
        ASSERT_EQ(55U, inspector.sequence_number());
    }
}

} // namespace test
} // namespace rcpt
} // namespace cheetah
} // namespace ska
