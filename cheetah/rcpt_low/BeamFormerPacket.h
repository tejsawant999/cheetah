/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_RCPT_LOW_BEAMFORMERPACKET_LOW_H
#define SKA_CHEETAH_RCPT_LOW_BEAMFORMERPACKET_LOW_H

#include "PacketSample.h"
#include "cheetah/data/Units.h"
#include "cheetah/rcpt_low/Config.h"
#include "ska/cbf_psr_interface/CbfPsrHeader.h"
#include <cstdlib>
#include <cstdint>
#include <array>

namespace ska {
namespace cheetah {
namespace rcpt_low {

/**
 * @brief
 *   Interface to packing/unpacking rcpt from the BeamFormer rcpt stream UDP packet
 *
 */


template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
class BeamFormerPacket
{

    public:
        enum class PacketType { PssLow, PssMid, PstLow, PstMid};

    private:
        typedef PacketSample<PacketDataType> Sample;
        typedef struct ska::cbf_psr_interface::CbfPsrHeader PacketHeader;
        typedef decltype(PacketHeader::packet_sequence_number) PacketNumberType;
        typedef decltype(PacketHeader::first_channel_number) ChannelNumberType;
        typedef decltype(PacketHeader::scan_id) ScanIdType;
        typedef decltype(PacketHeader::beam_number) BeamNumberType;
        typedef decltype(PacketHeader::data_precision) DataPrecisionType;
        typedef decltype(PacketHeader::magic_word) MagicWordType;
        typedef decltype(PacketHeader::channel_separation) ChannelSeperationType;
        typedef uint16_t WeightsType;

    public:
        BeamFormerPacket();
        ~BeamFormerPacket();

        /**
         * @brief the total size of the udp packets header
         */
        constexpr static std::size_t header_size();

        /**
         * @brief the total size of the udp packets footer
         */
        constexpr static std::size_t footer_size();

        /**
         * @brief the total size in bytes of the channel rcpt
         */
        constexpr static std::size_t payload_size();

        /**
         * @brief the total size in bytes of the channel rcpt
         */
        constexpr static std::size_t data_size();

        /**
         * @brief the total number of time samples in the packet
         */
        static std::size_t  number_of_time_samples();

        /**
         * @brief the total number of samples in the rcpt payload
         */
        constexpr static std::size_t number_of_samples();

        /**
         * @brief the total number of frequencey channels in the rcpt payload
         */
        static std::size_t number_of_channels();

        /**
         * @brief the total size of the packet (header + payload + footer)
         */
        constexpr static std::size_t size();

        /**
         * @brief set the counter in the header
         */
        void packet_count(PacketNumberType);

        /**
         * @brief get the counter info from header
         */
        PacketNumberType packet_count() const;

        /**
         * @brief set the type of packet
         */
        void packet_type(PacketType const);

        /**
         * @brief return the packet type
         */
        PacketType packet_type() const;

        /**
         * @brief insert a sample
         */
        void insert(std::size_t sample_number, Sample s);

        /**
         * @brief return the named sample
         */
        Sample const& sample(std::size_t sample) const;

        /**
         * @brief Pointers to the begin and end of the data in the packet
         */
        const Sample* begin() const;
        const Sample* end() const;

        /**
         * @brief Pointers to the begin and end of the weights in the packet
         */
        const WeightsType* begin_weights() const;
        const WeightsType* end_weights() const;

        /**
         * @brief return the maximum value the packet_count can take
         */
        static constexpr PacketNumberType max_sequence_number();

        /**
         * @brief the number of the first channel in the packet
         */
        ChannelNumberType first_channel_number() const;
        void first_channel_number(ChannelNumberType number);

        /**
         * @brief scan ID of the packet stream
         */
        ScanIdType scan_id() const;
        void scan_id(ScanIdType number);


        /**
         * @brief beam id of the packet stream
         */
        BeamNumberType beam_number() const;
        void beam_number(BeamNumberType number);

        /**
         * @brief First channel frequency in the packet in MHz
         */
        boost::units::quantity<data::MegaHertz, double> first_channel_frequency() const;
        void first_channel_frequency(boost::units::quantity<data::MegaHertz, double> value);

        /**
         * @brief Data precision
         */
        DataPrecisionType data_precision() const;
        void data_precision(DataPrecisionType number);

        /**
         * @brief scaling factor
        float scale() const;
        float scale(float number);
         */

        /**
         * @brief Magic word used for consistency checks.
         */
        MagicWordType magic_word() const;
        void magic_word(MagicWordType number);

        /**
         * @brief Number of channels in each packet.
         */
        auto channels_per_packet() const -> decltype(PacketHeader::channels_per_packet);
        void channels_per_packet(decltype(PacketHeader::channels_per_packet) number);

        /**
         * @brief Timestamp in seconds
         */
        auto timestamp_seconds() const -> decltype(PacketHeader::timestamp_seconds);
        void timestamp_seconds(decltype(PacketHeader::timestamp_seconds) number);

        /**
         * @brief Timestamp in attoseconds
         */
        auto timestamp_attoseconds() const -> decltype(PacketHeader::timestamp_attoseconds);
        void timestamp_attoseconds(decltype(PacketHeader::timestamp_attoseconds) number);

        /**
         * @brief channel_separation
         */
        ChannelSeperationType channel_separation() const;
        void channel_separation(ChannelSeperationType number);

        /**
         * @brief set all weights to unity
         */
        void set_unit_weights();

    private: // static variables
        static constexpr std::size_t _packet_data_size = ChannelsPerPacket*TimeSamplesPerPacket*sizeof(Sample)*2;
        static constexpr std::size_t _packet_weights_size = ChannelsPerPacket*sizeof(WeightsType);
        static constexpr std::size_t _packet_data_padding_size = (16-(_packet_data_size%16))%16;
        static constexpr std::size_t _packet_weights_padding_size = (16-(_packet_weights_size%16))%16;

    private:
        PacketHeader _header;
        static_assert(sizeof(_header)==96,"poor alignment detected in header");
        WeightsType _weights[(_packet_weights_size+_packet_weights_padding_size)/sizeof(WeightsType)];
        Sample _data[(_packet_data_size+_packet_data_padding_size)/sizeof(Sample)];
        static const std::size_t _number_of_samples = _packet_data_size/(sizeof(Sample));
        static const std::size_t _number_of_channels = _packet_weights_size/sizeof(uint16_t);
        static const std::size_t _size = _packet_data_padding_size + _packet_weights_padding_size + _packet_data_size + _packet_weights_size + sizeof(PacketHeader);
        static const std::size_t _number_of_time_samples = _packet_data_size/(2*sizeof(Sample)*_number_of_channels);

};


typedef BeamFormerPacket<int8_t, 128,9> BeamFormerPacketLow;
static_assert(sizeof(BeamFormerPacketLow)==4736,"BeamFormerPacketLow Packet size is not as expected");

static constexpr uint32_t number_of_channels_low = 7776;


} // namespace rcpt_low
} // namespace cheetah
} // namespace ska
#include "cheetah/rcpt_low/detail/BeamFormerPacket.cpp"

#endif // SKA_CHEETAH_RCPT_LOW_BEAMFORMERPACKET_LOW_H
