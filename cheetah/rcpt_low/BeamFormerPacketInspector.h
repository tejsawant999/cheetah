/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_RCPT_LOW_BEAMFORMERPACKETINSPECTOR_H
#define SKA_CHEETAH_RCPT_LOW_BEAMFORMERPACKETINSPECTOR_H

#include "cheetah/rcpt_low/BeamFormerPacket.h"

namespace ska {
namespace cheetah {
namespace rcpt_low {

/**
 * @brief
 *      BeamFormerPacket inspection and data extraction
 */
template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
class BeamFormerPacketInspector
{
    public:
        using Packet = BeamFormerPacket<PacketDataType, TimeSamplesPerPacket, ChannelsPerPacket>;
        typedef decltype(ska::cbf_psr_interface::CbfPsrHeader::packet_sequence_number) PacketNumberType;

    public:

        BeamFormerPacketInspector(Packet const& packet);
        ~BeamFormerPacketInspector();


        /**
         * @brief return the sequence number embedded in the packet
         */
        PacketNumberType sequence_number() const;

        /**
         * @brief return true if no further processing is required on this packet
         */
        bool ignore() const;

        inline BeamFormerPacket<PacketDataType, TimeSamplesPerPacket, ChannelsPerPacket> const& packet() const;

    private:
        Packet _packet;
};

typedef BeamFormerPacketInspector<int8_t,128,9> PacketInspectorLow;
} // namespace rcpt_low
} // namespace cheetah
} // namespace ska
#include "cheetah/rcpt_low/detail/BeamFormerPacketInspector.cpp"

#endif // SKA_CHEETAH_RCPT_LOW_BEAMFORMERPACKETINSPECTOR_H
