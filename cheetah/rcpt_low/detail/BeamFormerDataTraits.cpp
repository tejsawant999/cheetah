/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt_low/BeamFormerDataTraits.h"
#include <panda/Log.h>
#include <algorithm>


namespace ska {
namespace cheetah {
namespace rcpt_low {

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::BeamFormerDataTraits()
{
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::~BeamFormerDataTraits()
{

}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
uint64_t BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::sequence_number(PacketInspector const& packet)
{
    return packet.sequence_number();
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
std::size_t BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::chunk_size(DataType const& chunk)
{
    return chunk.number_of_spectra() * chunk.number_of_channels();
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
std::size_t BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::packet_size()
{
    return (PacketInspector::Packet::number_of_samples()/2);
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
std::size_t BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::data_size()
{
    return PacketInspector::Packet::data_size();
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
bool BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::align_packet(PacketInspector const& inspector)
{
    return inspector.packet().first_channel_number() == 0;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::packet_stats(uint64_t packets_missing, uint64_t packets_expected)
{
    if(packets_missing > 0)
    {
        PANDA_LOG_WARN << "missing packets: " << packets_missing << " in " << packets_expected;
    }
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
template<typename ContextType>
void BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::deserialise_packet(ContextType& context, PacketInspector const& packet)
{
    typedef PacketSample<PacketDataType> Sample;
    unsigned offset = context.offset();
    auto packet_it=packet.packet().begin() + context.packet_offset();
    auto weights_it = packet.packet().begin_weights();
    auto it=context.chunk().begin() + offset;

    uint32_t number_of_time_samples = packet.packet().number_of_time_samples();
    uint32_t number_of_channels = packet.packet().number_of_channels();


    for(uint32_t channel=0;channel<number_of_channels;++channel)
    {
        for(uint32_t sample=0; sample<number_of_time_samples; ++sample)
        {
            Sample temp_sample0 = (*(packet_it+((channel*number_of_time_samples*2+sample))));
            Sample temp_sample1 = (*(packet_it+((channel*number_of_time_samples*2+sample+number_of_time_samples))));
            *(it+(channel*number_of_time_samples+sample)) = (temp_sample0.intensity()+temp_sample1.intensity())*(*(weights_it+channel));
        }
    }
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
template<typename ContextType>
void BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::process_missing_slice(ContextType& context)
{
    //essentially replacing the data corresponding to the missing packet to 0. (Dont know what is the best approch here.)
    unsigned offset = context.offset();
    auto it=context.chunk().begin() + offset;
    PANDA_LOG_DEBUG << "processing missing packet: data=" << (void*)&*it << context;
    for(std::size_t i=0; i < context.size() ; ++i) {
        *it = 0;
        assert(it!=context.chunk().end());
        ++it;
    }
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
template<typename ResizeContextType>
void BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::resize_chunk(ResizeContextType& context)
{
    PANDA_LOG_DEBUG << "resizing data: " << context;
    // drops any incomplete spectra
    context.chunk().resize(data::DimensionSize<data::Time>(context.size()/context.chunk().number_of_channels()));
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr typename BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::PacketNumberType
BeamFormerDataTraits<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::max_sequence_number()
{
    return PacketInspector::Packet::max_sequence_number();
}

} // namespace rcpt_low
} // namespace cheetah
} // namespace ska
