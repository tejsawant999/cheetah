/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt_low/BeamFormerPacket.h"
#include <cassert>
#include <limits>


namespace ska {
namespace cheetah {
namespace rcpt_low {

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::header_size()
{
    return sizeof(PacketHeader);
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::footer_size()
{
    //return sizeof(Footer);
    return 0;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::payload_size()
{
    return _packet_data_size+_packet_weights_size+_packet_data_padding_size+_packet_weights_padding_size;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::data_size()
{
    return _packet_data_size;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::number_of_time_samples()
{
    return _number_of_time_samples;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::number_of_samples()
{
    return _number_of_samples;
}


template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::size()
{
    return _size;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
constexpr typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::PacketNumberType BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::max_sequence_number()
{
    return std::numeric_limits<PacketNumberType>::max();
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::BeamFormerPacket()
{
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::~BeamFormerPacket()
{
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
std::size_t BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::number_of_channels()
{
    return _number_of_channels;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::packet_count(PacketNumberType packet_count)
{
    _header.packet_sequence_number = packet_count;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::PacketNumberType BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::packet_count() const
{
    return _header.packet_sequence_number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::packet_type(PacketType const type)
{
    _header.packet_destination = static_cast<decltype(_header.packet_destination)>(type);
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::PacketType BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::packet_type() const
{
    return _header.packet_destination;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::insert(std::size_t sample_number, Sample s)
{
    assert(sample_number < number_of_samples());
    _data[sample_number] = std::move(s); // seems to be faster than using std::swap
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
PacketSample<PacketDataType> const& BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::sample(std::size_t sample_number) const
{
    return _data[sample_number];
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
const PacketSample<PacketDataType>* BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::begin() const
{
    return &_data[0];
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
const PacketSample<PacketDataType>* BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::end() const
{
    return &_data[_packet_data_size/sizeof(Sample)];
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
const typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::WeightsType* BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::begin_weights() const
{
    return &_weights[0];
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
const typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::WeightsType* BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::end_weights() const
{
    return &_weights[_packet_weights_size/sizeof(WeightsType)];
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::set_unit_weights()
{
    for(unsigned i=0; i<ChannelsPerPacket; ++i) _weights[i]=1;;
    return;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::ChannelNumberType BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::first_channel_number() const
{
    return _header.first_channel_number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::first_channel_number(ChannelNumberType number)
{
    _header.first_channel_number = number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::first_channel_frequency(boost::units::quantity<data::MegaHertz, double> value)
{
    _header.first_channel_frequency = static_cast<decltype(PacketHeader::first_channel_frequency)>(value.value()*1000);
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
boost::units::quantity<data::MegaHertz, double> BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::first_channel_frequency() const
{
    double frequency_value = ((double)_header.first_channel_frequency)/1000.0;

    return (frequency_value * boost::units::si::mega * boost::units::si::hertz);
}


template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::ScanIdType BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::scan_id() const
{
    return _header.scan_id;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::scan_id(ScanIdType number)
{
    _header.scan_id = number;
}

/**
* @brief beam id of the packet stream
*/
template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::BeamNumberType BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::beam_number() const
{
    return _header.beam_number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::beam_number(BeamNumberType number)
{
    _header.beam_number = number;
}


template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::MagicWordType
BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::magic_word() const
{
    return _header.magic_word;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::magic_word(MagicWordType number)
{
    _header.magic_word = number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
auto BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::timestamp_attoseconds() const -> decltype(PacketHeader::timestamp_attoseconds)
{
    return _header.timestamp_attoseconds;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::timestamp_attoseconds(decltype(PacketHeader::timestamp_attoseconds) number)
{
    _header.timestamp_attoseconds = number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
auto BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::timestamp_seconds() const -> decltype(PacketHeader::timestamp_seconds)
{
    return _header.timestamp_seconds;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::timestamp_seconds(decltype(PacketHeader::timestamp_seconds) number)
{
    _header.timestamp_seconds = number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::ChannelSeperationType
BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::channel_separation() const
{
    return _header.channel_separation;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::channel_separation(ChannelSeperationType number)
{
    _header.channel_separation = number;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::DataPrecisionType
BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::data_precision() const
{
    return _header.data_precision;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
void BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>
::data_precision(DataPrecisionType number)
{
    _header.data_precision = number;
}


} // namespace rcpt_low
} // namespace cheetah
} // namespace ska
