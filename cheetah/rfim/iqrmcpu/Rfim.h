#ifndef SKA_CHEETAH_IQRMCPU_RFIM_H
#define SKA_CHEETAH_IQRMCPU_RFIM_H

#include "cheetah/rfim/iqrmcpu/Config.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/rfim/RfimBase.h"
#include "cheetah/rfim/PolicyInfo.h"
#include "cheetah/rfim/policy/Policy.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/RfimFlaggedData.h"

namespace ska {
namespace cheetah {
namespace rfim {
namespace iqrmcpu {

/**
 * @brief
 *    A CPU implementation of the iqrmcpu algorithm
 *
 * @details
 *
 */

template<typename RfimTraits>
class Rfim: public RfimBase<Rfim<RfimTraits>, typename RfimTraits::Policy>
{
    typedef RfimBase<Rfim<RfimTraits>, typename RfimTraits::Policy> BaseT;
    friend BaseT;

    typedef typename RfimTraits::Policy Policy;
    typedef typename PolicyInfo<Policy>::AdapterType DataAdapter;

    public:
        typedef typename PolicyInfo<Policy>::ReturnType ReturnType;
        typedef cheetah::Cpu Architecture;

    public:
        Rfim(Config const& config);
        ~Rfim();

        using BaseT::operator();

    protected:
        template<typename DataType>
        void operator()(DataType const& data, DataAdapter& adpater);

    private:
        Config const& _config;
};


} // namespace iqrmcpu
} // namespace rfim
} // namespace cheetah
} // namespace ska
#include "detail/Rfim.cpp"

#endif // SKA_CHEETAH_IQRMCPU_RFIM_H
