/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SIFT_CONFIG_H
#define SKA_CHEETAH_SIFT_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/sift/simple_sift/Config.h"
#include "panda/PoolSelector.h"

namespace ska {
namespace cheetah {
namespace sift {

/**
 * @brief	Configuration for the sift module
 * 
 */
class Config : public cheetah::utils::Config
{
    public:
        Config();
        ~Config();

        /**
         * @brief Configuration details for the simple_sift algorithm
         */
        simple_sift::Config const& simple_sift_algo_config() const;

        /**
         * @brief The max number of harmonics to search
         */
        std::size_t num_candidate_harmonics() const;
        void num_candidate_harmonics(std::size_t const& num_candidate_harmonics);

        /**
         * @brief 
         */
        double match_factor() const;
        void match_factor(double const& match_factor);

        /**
         * @brief returns true if sift task is to be performed
         */
        bool active() const;

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _active;
        std::size_t _num_candidate_harmonics;
        double _match_factor;
        simple_sift::Config _simple_sift_config;

};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace sift
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_SIFT_CONFIG_H 
