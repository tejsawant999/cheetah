/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sift/Config.h"

namespace ska {
namespace cheetah {
namespace sift {

Config::Config()
    : cheetah::utils::Config("sift")
    , _active(true)
    , _num_candidate_harmonics(8)
    , _match_factor(0.001)
{
    add(_simple_sift_config);
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options("active", boost::program_options::value<bool>(&_active)->default_value(_active), "turn on/off the sift algo");
    add_options("num_candidate_harmonics", boost::program_options::value<std::size_t>(&_num_candidate_harmonics)->default_value(_num_candidate_harmonics), "The max harmonic to use when sifting.");
    add_options("match_factor", boost::program_options::value<double>(&_match_factor)->default_value(_match_factor), "Closeness factor to determine if a signal is a harmonic of a candidate.");
}

bool Config::active() const
{
    return _active;
}

std::size_t Config::num_candidate_harmonics() const
{
    return _num_candidate_harmonics;
}

void Config::num_candidate_harmonics(std::size_t const& num_candidate_harmonics)
{
    _num_candidate_harmonics = num_candidate_harmonics;
}

double Config::match_factor() const
{
    return _match_factor;
}

void Config::match_factor(double const& match_factor)
{
    _match_factor = match_factor;
}

simple_sift::Config const& Config::simple_sift_algo_config() const
{
    return _simple_sift_config;
}

} // namespace sift
} // namespace cheetah
} // namespace ska
