/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/astroaccelerate/Sps.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/SpCcl.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include "panda/Error.h"
#include <vector>
#include <memory>
#include <iostream>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {


#ifdef ENABLE_ASTROACCELERATE
template<typename DmHandler, typename SpHandler>
void Sps::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , BufferType& agg_buf
                    , DmHandler& dm_handler
                    , SpHandler& sp_handler
                    )
{
    auto it = this->_cuda_runner.find(gpu.device_id());
    if(it==this->_cuda_runner.end()) {
        TimeFrequencyType const& data=*(agg_buf.composition().front());
        set_dedispersion_strategy(this->_cuda_runner.at(0U).dedispersion_strategy().get_gpu_memory(), data, gpu.device_id());
        it=this->_cuda_runner.find(gpu.device_id());
    }
    (*it).second(gpu, agg_buf, dm_handler, sp_handler);
}

template<typename DmHandler, typename SpHandler, typename OtherBufferType>
void Sps::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , OtherBufferType&
                    , DmHandler&
                    , SpHandler&
                    )
{
    throw panda::Error("astroaccelerate::Sps can only handle uint8_t - please deactivate this algorithm in your config");
}
#endif //  ENABLE_ASTROACCELERATE

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska
