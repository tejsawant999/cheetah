#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/sps/test_utils/SpsTester.h"
#include <memory>
#include <vector>


namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {
namespace test {

struct AstroAccelerateTraits : public sps::test::SpsTesterTraits<sps::astroaccelerate::Sps::Architecture,sps::astroaccelerate::Sps::ArchitectureCapability>
{
    typedef sps::test::SpsTesterTraits<sps::astroaccelerate::Sps::Architecture, typename sps::astroaccelerate::Sps::ArchitectureCapability> BaseT;
    typedef typename BaseT::Arch Arch;
    void configure(sps::Config& config) override {
        BaseT::configure(config);
        auto& astroaccelerate_config = config.astroaccelerate_config();
        astroaccelerate_config.activate();
    }
};

} // namespace test
} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace sps {
namespace test {

typedef ::testing::Types<sps::astroaccelerate::test::AstroAccelerateTraits> AstroAccelerateTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, SpsTester, AstroAccelerateTraitsTypes);

} // namespace test
} // namespace sps
} // namespace cheetah
} // namespace ska
