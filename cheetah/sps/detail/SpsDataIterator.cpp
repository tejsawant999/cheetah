/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/detail/SpsDataIterator.h"
#include <algorithm>

namespace ska {
namespace cheetah {
namespace sps {

template<typename TimeFrequencyType, typename RfiFlagDataType>
SpsDataIterator<TimeFrequencyType, RfiFlagDataType>::SpsDataIterator( RfiFlagDataType& data, size_t offset)
    : _flag_it(data.rfi_flags().begin() + offset)
    , _tf_it(data.tf_data().begin() + offset)
    , _tf_end_it(data.tf_data().cend())
{
    assert(data.rfi_flags().data_size() == data.tf_data().data_size());
    // we take the median of the means for each channel as our default replacement value
    typedef typename RfiFlagDataType::Statistics Statistics;
    auto channel_stats = data.channel_stats(); // create a copy of the stats data
    std::size_t s = channel_stats.size()/2;
    std::nth_element(channel_stats.begin(), channel_stats.begin() + s, channel_stats.end(), [](Statistics const& s1, Statistics const& s2)
                                                                                            {
                                                                                                return s1.mean < s2.mean;
                                                                                            } );
    _value = channel_stats[s].mean;
    _replacement_value = _value;
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
SpsDataIterator<TimeFrequencyType, RfiFlagDataType>::~SpsDataIterator()
{
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
bool SpsDataIterator<TimeFrequencyType, RfiFlagDataType>::operator!=( SpsDataIterator<TimeFrequencyType, RfiFlagDataType> const& iterator) const
{
    return this->_tf_it != iterator._tf_it;
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
bool SpsDataIterator<TimeFrequencyType, RfiFlagDataType>::operator==( SpsDataIterator<TimeFrequencyType, RfiFlagDataType> const& iterator) const
{
    return this->_tf_it == iterator._tf_it;
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
SpsDataIterator<TimeFrequencyType, RfiFlagDataType>& SpsDataIterator<TimeFrequencyType, RfiFlagDataType>::operator++()
{
    ++_tf_it;
    ++_flag_it;
    if(_tf_it!=_tf_end_it) {
        if (*(_flag_it))
            _value = _replacement_value;
        else
            _value = *(_tf_it);
    }
    return *this;
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
typename SpsDataIterator<TimeFrequencyType, RfiFlagDataType>::const_reference SpsDataIterator<TimeFrequencyType, RfiFlagDataType>::operator*() const
{
    return _value;
}

} // namespace rfim
} // namespace cheetah
} // namespace ska
