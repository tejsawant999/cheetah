/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPS_SPSDATAITERATORFACTORY_H
#define SKA_CHEETAH_SPS_SPSDATAITERATORFACTORY_H

#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/RfimFlaggedData.h"
#include "cheetah/sps/detail/SpsDataIterator.h"
#include "panda/AggregationBufferFiller.h"

namespace ska {
namespace cheetah {
namespace sps {

/**
 * @brief
 * @details
 */
template<typename TimeFrequencyType>
class SpsDataIteratorFactory: public ska::panda::BeginEndFactory<TimeFrequencyType>
{
    public:
 	  typedef data::RfimFlaggedData<TimeFrequencyType> RfiFlagDataType;
	  typedef SpsDataIterator<TimeFrequencyType, RfiFlagDataType> Iterator;
	  typedef SpsDataIterator<const TimeFrequencyType, const RfiFlagDataType> ConstIterator;
    public:

        SpsDataIteratorFactory();
        ~SpsDataIteratorFactory();

        using ska::panda::BeginEndFactory<TimeFrequencyType>::begin;
        using ska::panda::BeginEndFactory<TimeFrequencyType>::end;

        inline static Iterator begin(RfiFlagDataType& data);
        inline static ConstIterator begin(RfiFlagDataType const& data);
        inline static Iterator end(RfiFlagDataType& data);
        inline static ConstIterator end(RfiFlagDataType const& data);

};

} // namespace sps
} // namespace cheetah
} // namespace ska
#include "cheetah/sps/detail/SpsDataIteratorFactory.cpp"
#endif // SKA_CHEETAH_SPS_SPSDATAITERATORFACTORY_H
