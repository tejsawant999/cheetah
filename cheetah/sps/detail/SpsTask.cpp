/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/detail/SpsTask.h"
#include "panda/TupleUtilities.h"
#include <limits>


namespace ska {
namespace cheetah {
namespace sps {


template<typename DmHandler, typename SpHandler, typename CommonTraits>
SpsTask<DmHandler, SpHandler, CommonTraits>::SpsTask(DmHandler dm_handler, SpHandler sp_handler)
    : _dm_handler(std::move(dm_handler))
    , _sp_handler(std::move(sp_handler))
{
}

template<typename DmHandler, typename SpHandler, typename CommonTraits>
SpsTask<DmHandler, SpHandler, CommonTraits>::~SpsTask()
{
}

template<typename DmHandler, typename SpHandler, typename CommonTraits>
template<typename DataType>
void SpsTask<DmHandler, SpHandler, CommonTraits>::call_dm_handler(DataType&& d) const
{
    return _dm_handler(std::forward<DataType>(d));
}

template<typename DmHandler, typename SpHandler, typename CommonTraits>
template<typename DataType>
void SpsTask<DmHandler, SpHandler, CommonTraits>::call_sp_handler(DataType&& d) const
{
    return _sp_handler(std::forward<DataType>(d));
}

template<typename DmHandler, typename SpHandler, typename CommonTraits>
std::shared_ptr<panda::ResourceJob> SpsTask<DmHandler, SpHandler, CommonTraits>::submit(BufferType)
{
    PANDA_LOG_WARN << "no sps algo defined";
    return std::make_shared<panda::ResourceJob>();
}


template<typename DmHandler, typename SpHandler, typename CommonTraits
        ,typename PoolType, typename... Algos>
SpecificSpsTask<DmHandler, SpHandler, CommonTraits, PoolType, Algos...>::SpecificSpsTask(
                        DmHandler dm_handler
                      , SpHandler sp_handler
                      , PoolType& pool
                      , Algos&&... algos)
    : BaseT(dm_handler, sp_handler)
    , _algos(std::forward<Algos>(algos)...)
    , _pool(&pool)
{
}

template<typename DmHandler, typename SpHandler, typename CommonTraits
        ,typename PoolType, typename... Algos>
std::shared_ptr<panda::ResourceJob> SpecificSpsTask<DmHandler, SpHandler, CommonTraits, PoolType, Algos...>::submit(BufferType buffer)
{
    std::shared_ptr<SpecificSpsTask> self = this->shared_from_this();
    return _pool->submit(self, std::move(buffer));
}

template<typename DmHandler, typename SpHandler, typename CommonTraits
        ,typename PoolType, typename... Algos>
template<typename Arch>
void SpecificSpsTask<DmHandler, SpHandler, CommonTraits, PoolType, Algos...>::operator()(panda::PoolResource<Arch>& resource, BufferType buffer)
{
    auto& algo = _algos.template get<Arch>();
    algo(resource, buffer, this->_dm_handler, this->_sp_handler);
}

namespace {
    template<typename Arch>
    struct BufferOverlap
    {
        template<typename AlgoTuple>
        inline void operator()(AlgoTuple const& algos, std::size_t& max_size)
        {
            std::size_t bs = algos.template get<Arch>().buffer_overlap();
            if(bs > max_size) {
                max_size = bs;
            }
        }
    };

    template<typename Arch>
    struct SetDedispersionStrategy
    {
        template<typename AlgoTuple, typename Data>
        inline void operator()(AlgoTuple& algos, std::size_t& memory, std::size_t memory_limit, Data const& data )
        {
            std::size_t mem = algos.template get<Arch>().set_dedispersion_strategy(memory_limit, data);
            if(mem < memory) memory = mem;
        }
    };
} // namespace

template<typename DmHandler, typename SpHandler, typename CommonTraits>
std::size_t SpsTask<DmHandler, SpHandler, CommonTraits>::buffer_overlap() const
{
    return 0;
}

template<typename DmHandler, typename SpHandler, typename CommonTraits
        ,typename PoolType, typename... Algos>
std::size_t SpecificSpsTask<DmHandler, SpHandler, CommonTraits, PoolType, Algos...>::buffer_overlap() const
{
    std::size_t buffer_overlap=0;
    panda::ForEach<typename ImplementationsType::Architectures, BufferOverlap>::exec(_algos, buffer_overlap);
    return buffer_overlap;
}

template<typename DmHandler, typename SpHandler, typename CommonTraits>
std::size_t SpsTask<DmHandler, SpHandler, CommonTraits>::set_dedispersion_strategy(
                                std::size_t memory_min
                               ,TimeFrequencyType const&)
{
    return memory_min;
}

template<typename DmHandler, typename SpHandler, typename CommonTraits
        ,typename PoolType, typename... Algos>
std::size_t SpecificSpsTask<DmHandler, SpHandler, CommonTraits, PoolType, Algos...>::set_dedispersion_strategy(
                                std::size_t memory_limit
                               ,TimeFrequencyType const& data)
{
    std::size_t memory=(memory_limit==0)?std::numeric_limits<std::size_t>::max():memory_limit;
    panda::ForEach<typename ImplementationsType::Architectures, SetDedispersionStrategy>::exec(_algos, memory, memory_limit, data);
    return memory;
}

template<typename DmHandler, typename SpHandler, typename CommonTraits>
void SpsTask<DmHandler, SpHandler, CommonTraits>::finish(BufferFillerType& agg_buf_filler)
{
    panda::FinishTask<SpsTask> finish(*this);
    agg_buf_filler.full_buffer_handler( [&](typename BufferFillerType::AggregationBufferType)
                                        {
                                            PANDA_LOG_WARN << "Sps has no implementation active";
                                        }
                                      );
    if(agg_buf_filler.flush())
    {
       finish.wait();
    }
}

template<typename DmHandler, typename SpHandler, typename CommonTraits
        ,typename PoolType, typename... Algos>
void SpecificSpsTask<DmHandler, SpHandler, CommonTraits, PoolType, Algos...>::finish(BufferFillerType& agg_buf_filler)
{
    panda::FinishTask<SpecificSpsTask> finish(*this);
    agg_buf_filler.full_buffer_handler( [&](typename BufferFillerType::AggregationBufferType buffer)
                       {
                           auto job = _pool->submit(finish, std::move(buffer));
                           job->wait();
                           if(job->status() != ska::panda::ResourceJob::JobStatus::Finished )
                           {
                               finish.finished();
                           }
                       }
    );
    if(agg_buf_filler.flush())
    {
       finish.wait();
    }
}


} // namespace sps
} // namespace cheetah
} // namespace ska
