/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/test/SpsDataIteratorFactoryTest.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/RfimFlaggedData.h"

namespace ska {
namespace cheetah {
namespace sps {
namespace test {


SpsDataIteratorFactoryTest::SpsDataIteratorFactoryTest()
    : ::testing::Test()
{
}

SpsDataIteratorFactoryTest::~SpsDataIteratorFactoryTest()
{
}

void SpsDataIteratorFactoryTest::SetUp()
{
}

void SpsDataIteratorFactoryTest::TearDown()
{
}

TEST_F(SpsDataIteratorFactoryTest, test_returned_flagged_data_iterator)
{
    std::shared_ptr<data::TimeFrequency<Cpu, uint8_t>> data_ptr(new data::TimeFrequency<Cpu, uint8_t>(data::DimensionSize<data::Time>(1000), data::DimensionSize<data::Frequency>(10)));
    data::RfimFlaggedData<data::TimeFrequency<Cpu, uint8_t>> flagged_data(data::DimensionSize<data::Time>(1000), data::DimensionSize<data::Frequency>(10));
    SpsDataIteratorFactory<data::TimeFrequency<Cpu, uint8_t>> it_factory;
    auto it = it_factory.begin(flagged_data);
    SpsDataIterator<data::TimeFrequency<Cpu, uint8_t>, decltype(flagged_data)> it1(flagged_data, 0);
    ASSERT_TRUE(it1 == it);
    auto it_end = it_factory.end(flagged_data);
}

TEST_F(SpsDataIteratorFactoryTest, test_returned_tf_data_iterator)
{
    data::TimeFrequency<Cpu, uint8_t> data(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(1024));
    SpsDataIteratorFactory<decltype(data)> it_factory;
    auto it = it_factory.begin(data);
    ASSERT_EQ(data.begin(), it);
}

TEST_F(SpsDataIteratorFactoryTest, test_returned_const_tf_data_iterator)
{
    const data::TimeFrequency<Cpu, uint8_t> data(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(1024));
    SpsDataIteratorFactory<decltype(data)> it_factory;
    auto it = it_factory.begin(data);
    ASSERT_EQ(data.begin(), it);
}

/*TEST_F(SpsDataIteratorFactoryTest, test_correct_returned_value)
{
    std::shared_ptr<data::TimeFrequency<Cpu, uint8_t>> data_ptr(new data::TimeFrequency<Cpu, uint8_t>(data::DimensionSize<data::Time>(1000), data::DimensionSize<data::Frequency>(10)));
    std::fill((*data_ptr).begin(), (*data_ptr).end(), 1U);
    data::RfimFlaggedData<data::TimeFrequency<Cpu, uint8_t>> flagged_data((*data_ptr));
    // Set all flags to true
    flagged_data.rfi_flags().reset(true);
    SpsDataIterator<data::TimeFrequency<Cpu, uint8_t>, decltype(flagged_data)> it(flagged_data, 0);

    // Check if we get the correct numerical rep from the full chunk
    for (std::uint32_t ii=0; ii < 10000; ++ii)
    {
        ASSERT_EQ(*it, 0U);
        ++it;
    }

    flagged_data.rfi_flags().reset(false);
    SpsDataIterator<data::TimeFrequency<Cpu, uint8_t>, decltype(flagged_data)> it2(flagged_data, 0);
    SpsDataIteratorFactory<data::TimeFrequency<Cpu, uint8_t>> it_factory;
    auto it_p = it_factory.end(flagged_data);

    // Check if we get the correct numerical rep from the full chunk
    while (it2 != it_p )
    {
        ASSERT_EQ(*it2, 1U);
        ++it2;
    }
}*/

TEST_F(SpsDataIteratorFactoryTest, test_end_iterator)
{
    std::shared_ptr<data::TimeFrequency<Cpu, uint8_t>> data_ptr(new data::TimeFrequency<Cpu, uint8_t>(data::DimensionSize<data::Time>(1000), data::DimensionSize<data::Frequency>(10)));
    std::fill((*data_ptr).begin(), (*data_ptr).end(), 1U);
    data::RfimFlaggedData<data::TimeFrequency<Cpu, uint8_t>> flagged_data((*data_ptr));
    SpsDataIteratorFactory<data::TimeFrequency<Cpu, uint8_t>> it_factory;
    auto it = it_factory.end(flagged_data);

    SpsDataIterator<data::TimeFrequency<Cpu, uint8_t>,decltype(flagged_data)> it2(flagged_data, flagged_data.tf_data().data_size());
    ASSERT_EQ(it,it2);
}

} // namespace test
} // namespace sps
} // namespace cheetah
} // namespace ska
