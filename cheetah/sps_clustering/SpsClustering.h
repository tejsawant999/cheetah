/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPS_CLUSTERING_SPSCLUSTERING_H
#define SKA_CHEETAH_SPS_CLUSTERING_SPSCLUSTERING_H

#include "cheetah/sps_clustering/Config.h"
#include "cheetah/sps_clustering/Fof.h"
#include "cheetah/data/SpCcl.h"

namespace ska {
namespace cheetah {
namespace sps_clustering {

/**
 * @brief   A class that will merge candidates that have been labeled in the same group
 * @details Will take SpCcl object as an argument and return the same object
 */
class SpsClustering
{
        typedef Fof ClusteringAlgo;

    public:
        /**
         * @brief consructor
         */
        SpsClustering(Config const& config);
        ~SpsClustering();

        /**
         * @brief remove duplicate candidates
         *  @details cnadidates are considered to be duplicates if they are clustered together within the
         *           bounds provided by the configuration
         *  @return a new shared_ptr<SpCcl> candidate object with representative candidate fro each cluster.  The representative is the candidate
         *          with the maximum signal to noise ratio
         */
        template<typename NumRepType>
        std::shared_ptr<data::SpCcl<NumRepType>> operator()(std::shared_ptr<data::SpCcl<NumRepType>> const& cands);

    private:
        Config const& _config;
        ClusteringAlgo _clustered_candidates;

};


} // namespace sps_clustering
} // namespace cheetah
} // namespace ska
#include "cheetah/sps_clustering/detail/SpsClustering.cpp"
#endif // SKA_CHEETAH_SPS_CLUSTERING_SPSCLUSTERING_H
