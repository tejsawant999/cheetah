#include "cheetah/tdrt/cuda/Tdrt.cuh"
#include "cheetah/tdrt/cuda/TdrtMap.cuh"
#include "cheetah/cuda_utils/cuda_thrust.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace tdrt {
namespace cuda {


template <typename T, typename Alloc>
void Tdrt::process(ResourceType& gpu,
    data::TimeSeries<Architecture,T,Alloc> const& input,
    data::TimeSeries<Architecture,T,Alloc>& output,
    data::AccelerationType acceleration)
{
    typedef thrust::counting_iterator<std::size_t> CountIt;
    typedef thrust::transform_iterator< TdrtMap, CountIt > MapIt;
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    output.resize(input.size());
    CountIt begin(0);
    MapIt iter(begin, TdrtMap(acceleration, input.size(), input.sampling_interval()));
    thrust::gather(thrust::cuda::par,
        iter,
        iter+input.size(),
        input.begin(),
        output.begin());
    output.sampling_interval(input.sampling_interval());
}

} //cuda
} //tdrt
} //cheetah
} //ska