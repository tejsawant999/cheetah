#include "cheetah/tdrt/cuda/Tdrt.cuh"

namespace ska {
namespace cheetah {
namespace tdrt {
namespace cuda {

Tdrt::Tdrt(Config const& config, tdrt::Config const& algo_config)
    : utils::AlgorithmBase<Config, tdrt::Config>(config,algo_config)
{
}

Tdrt::~Tdrt()
{
}

} //cuda
} //tdrt
} //cheetah
} //ska
