set(module_utils_lib_src_cpu
    src/Config.cpp
    src/ConvolvePlan.cpp
    src/System.cpp
    src/TerminateException.cpp
    src/Version.cpp
    PARENT_SCOPE
)

TEST_UTILS()

add_subdirectory(test)
