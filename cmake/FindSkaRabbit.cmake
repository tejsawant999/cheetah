# Find the native SKA_RABBIT includes and library
#
#  SKA_RABBIT_INSTALL_DIR - top where rabbit framwork has been installed (lib and include dirs included)
#  SKA_RABBIT_LIBRARY_DIR - explicitly define directory where to find rabbit libraries
#  SKA_RABBIT_INCLUDE_DIR - where to find rabbit includes
#  SKA_RABBIT_LIBRARIES   - List of libraries when using rabbit.
#  SKA_RABBIT_FOUND       - True if rabbit found.

option(ENABLE_SKA_RABBIT "enable modules that utilise the ska rabbit library (fpga images)" false)

if(ENABLE_SKA_RABBIT)
    # required dependencies for ska rabbit
    set(ENABLE_OPENCL true CACHE FORCE "" BOOL)

IF (SKA_RABBIT_INCLUDE_DIR)
    SET(SKA_RABBIT_INC_DIR ${SKA_RABBIT_INCLUDE_DIR})
    UNSET(SKA_RABBIT_INCLUDE_DIR)
ENDIF (SKA_RABBIT_INCLUDE_DIR)

FIND_PATH(SKA_RABBIT_INCLUDE_DIR rabbit/Version.h
    PATHS ${SKA_RABBIT_INC_DIR}
	  ${SKA_RABBIT_INSTALL_DIR}/include
	  /usr/local/include
	  /usr/include )
message("Found ${SKA_RABBIT_INCLUDE_DIR} : ${SKA_RABBIT_INSTALL_DIR}")

SET(SKA_RABBIT_NAMES rabbit)
FOREACH( lib ${SKA_RABBIT_NAMES} )
    FIND_LIBRARY(SKA_RABBIT_LIBRARY_${lib}
	NAMES ${lib}
	PATHS ${SKA_RABBIT_LIBRARY_DIR} ${SKA_RABBIT_INSTALL_DIR} ${SKA_RABBIT_INSTALL_DIR}/lib /usr/local/lib /usr/lib
    )
    LIST(APPEND SKA_RABBIT_LIBRARIES ${SKA_RABBIT_LIBRARY_${lib}})
ENDFOREACH(lib)

# handle the QUIETLY and REQUIRED arguments and set SKA_RABBIT_FOUND to TRUE if.
# all listed variables are TRUE
INCLUDE(FindPackageHandleCompat)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SKA_RABBIT DEFAULT_MSG SKA_RABBIT_LIBRARIES SKA_RABBIT_INCLUDE_DIR)

IF(NOT SKA_RABBIT_FOUND)
    SET( SKA_RABBIT_LIBRARIES )
ELSE(NOT SKA_RABBIT_FOUND)
    add_definitions(-DENABLE_SKA_RABBIT)
ENDIF(NOT SKA_RABBIT_FOUND)

MARK_AS_ADVANCED(SKA_RABBIT_LIBRARIES SKA_RABBIT_INCLUDE_DIR)
endif(ENABLE_SKA_RABBIT)
