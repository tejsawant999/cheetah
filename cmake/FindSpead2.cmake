# Find the native SPEAD includes and library
#
#  Can request specific components (default is all components)
#  If all the requested components have been found, SPEAD_FOUND will be set to true
#  Components:
#    libs - The spead2 libraries and header files
#            This will set the following variables:
#               SPEAD_LIBRARIES   - List of libraries when using spead2.
#               SPEAD_LIBRARY_DIR - directory where spead2 libraries were found
#               SPEAD_INCLUDE_DIR - directory where spead2 includes were found
#
#    Example:   find_package(Panda REQUIRED cmake) - Would only require Panda Source directory to be present.
#
#
#  The following variables can be set to explicitly specify the locations of spead2 components in order of precedence
#  SPEAD_LIBRARY_DIR - explicitly define directory where to find spead2 libraries
#  SPEAD_INCLUDE_DIR - where to find spead2 includes
#  SPEAD_INSTALL_DIR - Top where spead2 framwork has been installed (lib and include dirs included)
option(ENABLE_SPEAD2 "Enable SPEAD2" OFF)

IF (ENABLE_SPEAD2)
 INCLUDE(FindPackageHandleCompat)

# -- include files
 IF (SPEAD_INCLUDE_DIR)
    SET(SPEAD_INC_DIR ${SPEAD_INCLUDE_DIR})
    UNSET(SPEAD_INCLUDE_DIR)
 ENDIF (SPEAD_INCLUDE_DIR)
 FIND_PATH(SPEAD_INCLUDE_DIR spead2/send_heap.h
    PATHS ${SPEAD_INC_DIR}
      ${SPEAD_INSTALL_DIR}/include
      /usr/local/include
      /usr/include )

# -- libraries
 SET(SPEAD_NAMES spead2)
 FOREACH( lib ${SPEAD_NAMES} )
    FIND_LIBRARY(SPEAD_LIBRARY_${lib}
    NAMES ${lib}
    PATHS ${SPEAD_LIBRARY_DIR} ${SPEAD_INSTALL_DIR} ${SPEAD_INSTALL_DIR}/lib /usr/local/lib /usr/lib
    )
    LIST(APPEND SPEAD_LIBRARIES ${SPEAD_LIBRARY_${lib}})
 ENDFOREACH(lib)

# handle the QUIETLY and REQUIRED arguments and set SPEAD_FOUND to TRUE if.
# all listed variables are TRUE
 FIND_PACKAGE_HANDLE_STANDARD_ARGS(SPEAD DEFAULT_MSG SPEAD_LIBRARIES SPEAD_INCLUDE_DIR)

 MARK_AS_ADVANCED(SPEAD_LIBRARIES SPEAD_INCLUDE_DIR)
 if(SPEAD_FOUND)
    message("Found spead2 library:")
    message("   SPEAD INCLUDE_DIR : ${SPEAD_INCLUDE_DIR}")
    message("   SPEAD LIBRARIES : ${SPEAD_LIBRARIES}")
    SET(CMAKE_CXX_FLAGS "-DENABLE_SPEAD ${CMAKE_CXX_FLAGS}")
 else(SPEAD_FOUND)
    SET( SPEAD_LIBRARIES )
    message("spead2 library not Found. Please specify one or more of SPEAD_INCLUDE_DIR, SPEAD_LIBRARY_DIR, SPEAD_INSTALL_DIR")
 endif(SPEAD_FOUND)

ENDIF(ENABLE_SPEAD2)
