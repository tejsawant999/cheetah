option(DEEP_TESTING "Enable extended testing requiring external data sets not available in this repository." OFF)
if(DEEP_TESTING)
    add_definitions(-DDEEP_TESTING)
    message( "DEEP_TESTING activated")
endif(DEEP_TESTING)
