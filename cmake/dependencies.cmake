include(cmake/thirdparty.cmake)
set(THIRDPARTY_DIR "${PROJECT_SOURCE_DIR}/thirdparty")
set(THIRDPARTY_BINARY_DIR "${CMAKE_BINARY_DIR}/thirdparty")

if(CMAKE_BUILD_TYPE MATCHES documentation)
  # -- only building the documentation, so only doc dependencies required
  list(APPEND CMAKE_MODULE_PATH "${PANDA_CMAKE_MODULE_PATH}")
  include(cmake/cuda.cmake)

else(CMAKE_BUILD_TYPE MATCHES documentation)
    # thirdparty dependencies
    include(cmake/googletest.cmake)
    include(compiler_settings)

    find_package(Panda REQUIRED)
    list(APPEND CMAKE_MODULE_PATH "${PANDA_CMAKE_MODULE_PATH}")
    include(cmake/boost.cmake)
    include(cmake/cuda.cmake)

    find_package(CbfPsrInterface REQUIRED)
    include_directories(SYSTEM ${CBF_PSR_INTERFACE_INCLUDE_DIR})

    find_package(AstroTypes REQUIRED) # depends on boost
    include_directories(SYSTEM ${ASTROTYPES_INCLUDE_DIR})

    find_package(AstroAccelerate)
    find_package(FFTW)
    find_package(PSRDADA)
    find_package(Spead2)
    find_package(SkaRabbit)

    # opencl is an optional rabbit dependency
    include(${PANDA_CMAKE_MODULE_PATH}/opencl.cmake)

    include_directories(SYSTEM ${Boost_INCLUDE_DIR}
                               ${PANDA_INCLUDE_DIR})

    if(FFTW_FOUND)
        include_directories(SYSTEM ${FFTW_INCLUDES})
    endif(FFTW_FOUND)
    if(ASTROACCELERATE_FOUND)
        include_directories(SYSTEM ${ASTROACCELERATE_INCLUDE_DIR})
    endif(ASTROACCELERATE_FOUND)
    if(SPEAD_FOUND)
        include_directories(SYSTEM ${SPEAD_INCLUDE_DIR})
    endif(SPEAD_FOUND)
    if (PSRDADA_FOUND)
        include_directories(SYSTEM ${PSRDADA_INCLUDE_DIR})
    endif(PSRDADA_FOUND)
    if (SKA_RABBIT_FOUND)
        include_directories(SYSTEM ${SKA_RABBIT_INCLUDE_DIR})
    endif(SKA_RABBIT_FOUND)
    if (OpenCL_FOUND)
        include_directories(SYSTEM ${OpenCL_INCLUDE_DIR})
    endif(OpenCL_FOUND)

    set(DEPENDENCY_LIBRARIES
        ${PANDA_LIBRARIES}
        #${ASTROTYPES_LIBRARIES}
        ${SPEAD_LIBRARIES}
        ${Boost_LIBRARIES}
        ${FFTW_LIBRARIES}
        ${ASTROACCELERATE_LIBRARIES}
        ${PSRDADA_LIBRARIES}
        ${CUDA_DEPENDENCY_LIBRARIES}
        ${OpenCL_LIBRARIES}
        ${SKA_RABBIT_LIBRARIES}
        )

endif(CMAKE_BUILD_TYPE MATCHES documentation)

# -- common depemdemcies
include(doxygen)
