#include <cinttypes>

namespace ska {
namespace cbf_psr_interface {

constexpr auto n_stokes_params = 4;
struct CbfPsrHeader {
    uint64_t packet_sequence_number;
    uint64_t timestamp_attoseconds;
    uint32_t timestamp_seconds;
    uint32_t channel_separation;
    uint64_t first_channel_frequency;
    float scale[n_stokes_params];
    uint32_t first_channel_number;
    uint16_t channels_per_packet;
    uint16_t valid_channels_per_packet;
    uint16_t number_of_time_samples;
    uint16_t beam_number;
    uint32_t magic_word;
    uint8_t packet_destination;
    uint8_t data_precision;
    uint8_t number_of_power_samples_averaged;
    uint8_t number_of_time_samples_weight;
    uint8_t oversampling_ratio_numerator;
    uint8_t oversampling_ratio_denominator;
    uint16_t beamformer_version;
    uint64_t scan_id;
    float offset[n_stokes_params];
};

} // namespace cbf_psr_interface
} // namespace ska
